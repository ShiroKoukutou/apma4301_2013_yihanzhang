% English Version of fe-i-tex

\documentclass[11pt]{article}

% Set margins:
\hoffset = -0.9in
\voffset = -0.75in
\textheight = 680pt
\textwidth = 470pt

% Indent all paragraphs
\usepackage{indentfirst}

% Mathematic libraries
\usepackage{amsmath}
\usepackage{amssymb}
\newcommand{\diff}{\mathrm{d}}
\newcommand{\Diff}{\mathrm{D}}
\newcommand{\me}{\mathrm{e}}
\newcommand{\mi}{\mathrm{i}}
\newcommand{\erf}{\mathrm{erf}}

% Symbol libraries
\usepackage{textcomp}

% Code display library
\usepackage{mylistings}

% Graphic libraries
\usepackage{float}
\usepackage{graphicx}

% Enumerate label libraries
\usepackage{enumitem}

% Font package
\usepackage{times}

\usepackage[compact]{titlesec}
\titleformat{\section}{\Large\rmfamily\scshape}{\thesection\quad}{0pt}{}[]

\usepackage{setspace}

\setlength{\parindent}{3ex}

% TODO: Add your article here.

\begin{document}

\title{ {\huge Numerical Method for Partial Differential Equations}\\[5pt] Final Project}
\author{Yihan Zhang, Bing Song}
\maketitle

\section{Introduction}

\begin{enumerate}[label=\arabic*., font=\bfseries, listparindent=\parindent]
	
	\item {\bf Background of the project}

		Diabetes is the seventh leading cause of death in the U.S., an underestimate since diabetes contributes to many diseases that are listed as primary causes of death. Close monitoring and timely correction of problematic blood glucose patterns can reduce the risk of acute hypoglycemia, hyperglycemia and diabetes-related complications; tight control of glycemia reduces eye, kidney, nerve and vascular diseases by 50\% or more among Type I diabetics, with similar results for Type II diabetes patients.

		A MEMS affinity glucose sensor uses a synthetic polymers, which measured glucose binding-induced changes in the permittivity of polymer solution. The dielectric device consist of a microchamber filled with affinity polymer solution and capped with a semipermeable membrane, which, while preventing the polymer from escaping, allows free glucose permeation.
		
		The aim of our project is to simulate the glucose diffusion in this device.

	\item {\bf Build up the problem}
		
		In order to simulate the behavior of liquid flow inside the device, first we need to figure out what mathematical problem we need to solve, including the geometry we want to simulate, and the metrics we want to measure. Therefore, an investigate of the principle of this device is needed. 
		
		The dielectric glucose sensor consists two electrodes inside a micro-chamber sealed by a cellulose semi-permeable membrane. One electrode is on the top of the silicon base and the other is embedded in a perforated diaphragm that is supported by several anti-stiction posts(Fig 1). While testing the dielectric sensor, it is fixed into a flow cell (around $10\times10\times5 mm^3$, not shown in Fig 1). Water is first injected into the flow cell and then glucose solutions of different concentrations are introduced.

		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.25]{./FigureForReport/fig1.png}
			\caption{Schematic of dielectirc sensor}
		\end{figure}

		\indent In order to simulate the liquid flow, a simplified geometry is applied in the project(Fig. 2). We set the dimensions approximately the same as that for experiment, where the width of the device is $10mm$, the height of the device is $2.2mm$ the thickness of the membrane is $0.2mm$ and the height of the flow cell is attached to the top of the device with an height of $7.8mm$. In real application environment, the velocity of the flow injected in to the device is $v\approx 10^{-4}cm/s$, and the concentration of glucose is about $G = 2\times 10^{-4}g/cm^3$.

		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.3]{./FigureForReport/fig2.png}
			\caption{Schematic of simplified geometry}
		\end{figure}

		Next we need to describe the behavior of the selective membrane. The selective membrane in real case, could filter glucose molecules out from other molecules in the water solution. Thus if we do not include any other solute inside fluid, we could just model the membrane as a region that the diffusivity of glucose is not the same as that in water solution. Also, because of the material, there should be no flow inside the region of the membrane.

		With this simplification of the problem, we can now set the objective of the project is to simulate the glucose diffusion problem in the geometry above, with different flow behavior and glucose diffusivity in different regions. In order to verify that the device is working correctly, we could measure the mean glucose concentration at the bottom of the device, to see how long time it takes for glucose to diffuse sufficiently.

\end{enumerate}
		
\section{Mathematical Model of the Problem}

\begin{enumerate}[label=\arabic*., font=\bfseries, listparindent=\parindent]
		
	\item {\bf Formulating PDE's to solve the problem}

	In order to solve this problem mathematically, we must first analyze the property of the liquid flow. In this flow system, we could first find the conservation of mass
		\begin{align*}
			\frac{D\rho}{Dt} + \rho\nabla\cdot\vec{v} = & 0
		\end{align*}

	Where $\rho$ is the density of the fluid, $t$ is the time and $\vec{v}$ is the velocity of the fluid.

	Consider a control volume inside the chamber. The total time rate of change of the glucose molecule inside the control volume equals to the sum of:
		\begin{itemize}
			\item Total amount of glucose convected through the boundary due to the movement of the liquid;
			\item Total amount of glucose diffused through the boundary due to the difference of concentration.
		\end{itemize}
		\hspace{2ex} Thus assume the concentration of glucose in the liquid is $G$, we have:
		$$
		\frac{\diff}{\diff t}\int_\Omega G\diff V = -\int_{\partial\Omega} G\vec{v}\cdot\hat{n}\diff S + \int_{\partial\Omega} D\nabla G\cdot\hat{n}\diff S
		$$

		Where $D$ is the diffusivity of glucose inside water. By converting it into volume integrals, we could then get the partial differential equation describing the behaviour of diffusion and convection of glucose inside water:
		$$
		\frac{\partial G}{\partial t} + \vec{v}\cdot\nabla G = \nabla\cdot(D\nabla G)
		$$

		Moreover, for the behavior of fluid inside the device, assume it is incompressible, we have Navier-Stokes equation:
		$$
		\rho\frac{\Diff \vec{v}}{\Diff t} = -\nabla P +\nabla\cdot\frac{\mu}{2}\big(\nabla\vec{v}+(\nabla\vec{v})^T\big)+\rho\vec{g}
		$$

		However, as there is no fluids flowing inside the membrane area, this equation will not be solved there.

		Using all three equations above, we are able to describe the behavior of flow and glucose inside the device mathematically.

	\item {\bf Simplification of the equations}

		The first approximation is that we could assume viscosity is constant every where, and diffusivity is constant inside both water and membrane area. However, the value of diffusivity is not the same in two areas. The diffusion equation now becomes:
		$$
		\frac{\partial G}{\partial t} + \vec{v}\cdot\nabla G = D\nabla^2 G
		$$

		The second approximation to be made to simplify the equations is the Boussinesq approximation. We assume the density is constant except for the gravity term. This is valid as the density of glucose solution in the real problem varies no larger than 1\textperthousand for different concentration of glucose. Thus for the gravity term, we assume a linear relationship between $G$ and $\rho$:
		$$
		\rho = \rho_0(1+\beta(G-G_0))= \rho_0(1+\beta G) \qquad \text{As}\ G_0 = 0
		$$

		Now, the equation of conservation of mass becomes:
		$$
		\nabla\cdot\vec{v} = 0
		$$

		Also, if we subtract the part that is proportional to the depth of water out of pressure, which means:
		$$
		P_{(new)} = P-\rho_0gy
		$$

		The Navier-Stokes equation now becomes:
		$$
		\rho_0 \frac{\Diff\vec{v}}{\Diff t} + \nabla P = \frac{\mu}{2}\nabla\cdot\big(\nabla\vec{v}+(\nabla\vec{v})^T\big) + \rho_0 g\beta G\hat{y}
		$$

	\item {\bf Scaling of the equations}
		
		In order to reduce the number of constants, and simplify the solution, we need to scale all the parameters to dimensionless variables. For now, we require the scale of the region is $x=10mm$ for the real problem, fluid velocity at entrance is $v=1\times 10^-4 cm/s$, and maximum concentration at the entrance is $G = 2\times 10^{-4}g/cm^3$. Thus we could assume:
		\begin{align*}
			x = & hx_{(new)} &
			\vec{v} = & v_0\vec{v}_{(new)}\\
			G = & G_0G_{(new)} &
			t = & t_0t_{(new)}\\
			P = & P_0P_{(new)} &
			\mu = & \mu_0\mu_{(new)}
		\end{align*}

		And we want new $x$ in range of $[0,5]$, $\vec{v}$ and $G$ with uniform range. Thus we could find:
		\begin{align*}
			h = & 0.2cm\\
			v_0 = & 10^{-4}cm/s\\
			G_0 = & 2\times 10^{-4}g/cm^3
		\end{align*}

		Then the diffusion equation turns into:
		$$
		\frac{1}{t_0}\frac{\partial G}{\partial t} + \frac{v_0}{h}\vec{v}\cdot\nabla G = \frac{D}{h^2}\nabla^2 G
		$$

		If we let:
		$$\frac{1}{t_0} = \frac{v_0}{h}$$

		Then we have:
		$$t_0 = \frac{h}{v_0} = 2\times 10^3s$$

		And we need a new diffusivity:
		$$D_{(new)} = \frac{Dt_0}{h^2}$$

		to make the diffusion equation dimensionless:
		$$
		\frac{\partial G}{\partial t} + \vec{v}\cdot\nabla G = D \nabla^2 G
		$$

		As diffusivity in water and in membrane are approximately equal to $6.35\times 10^{-6}cm^2/s$ and $2.15\times 10^{-6}cm^2/s$ respectively, the new dimensionless diffusivity are:
		\begin{align*}
			D_{water} = & 0.3175 & D_{membrane} = 0.1075
		\end{align*}

		As these numbers are about order of magnitude 1, we should include the diffusion term in the equation when solving it.

		After scaling, the Navier-Stokes equation then becomes:
		$$
		\frac{v_0}{t_0}\frac{\Diff\vec{v}}{\Diff\vec{t}} + \frac{P_0}{h}\nabla P = \frac{\mu_0v_0}{2h^2}\mu\nabla\cdot\big(\nabla\vec{v}+(\nabla\vec{v})^T\big) + \rho g\beta G_0 G\hat{y}
		$$

		As we want the new $\mu$ equals to 1, $\mu_0 = 8.9\times 10^{-3}g/cm\cdot s$. If we let:
		$$\frac{P_0}{h} = \frac{\mu_0v_0}{2h^2}$$

		Then we have:
		$$P_0 = \frac{\mu_0v_0}{2h} = 2.23\times 10^{-6}g/cm\cdot s^2$$

		We could call the coefficient before the material derivative of $\vec{v}$ some number 1 (denoted as $\mathrm So_1$), and that before $G$ is some number 2 (denoted as $\mathrm So_2$) after making the equation dimensionless. Then we could find:
		\begin{align*}
			\mathrm So_1 = & \frac{2\rho_0hv_0}{\mu_0} = 4.5\times 10^{-3}\\
			\mathrm So_2 = & \frac{2\rho_0\beta G_0gh^2}{\mu_0v_0} = 1.76\times 10^2
		\end{align*}

		Where $\beta\approx1$ according to real case. We could find the acceleration term has tiny impact on the system. Also, if we are solving for a steady state, there is no acceleration of velocity. Thus we could neglect this term for simplicity, and denote the only constant number in the equation as $\mathrm So$. Thus the dimensionless Navier-Stokes equation is:
		$$
		\nabla P = \mu\nabla\cdot\big(\nabla\vec{v}+(\nabla\vec{v})^T\big)+{\mathrm So}G\hat{y}
		$$

		All in all, the equation we need to solve is now
		\begin{align*}
			\nabla\cdot\vec{v} = & 0\\
			\frac{\partial G}{\partial t} + \vec{v}\cdot\nabla G = & D \nabla^2 G\\
			\nabla P = \mu\nabla\cdot\big(\nabla\vec{v}+(\nabla\vec{v})^T\big) & + {\mathrm So}G\hat{y}
		\end{align*}

	\item {\bf Boundary condition}

		In order to simulate the behavior of the device, we need to add appropriate boundary conditions to the mathematical problem. For both the concentration of glucose and velocity of the fliud, we could set the value is constant on the entrance. Moreover, there should be no penetration against all boundaries except for the exit. Thus the perpendicular part of velocity should be zero on those walls. Considering the behavior of the membrane, the velocity penetrating that area should be limited. However it is hard to implement. In this project, we will use no penetration from upper bound, as Dirichlet boundary condition on both sides of the membrane will cause pressure to blow up at certain point, making the problem unsolvable. The velocity at the bottom is, as well, small in compare with that in the top region. Thus the penetration at the boundary could be negligible.

\end{enumerate}

\section{Choice of Numerical Method}

\begin{enumerate}[label=\arabic*., font=\bfseries, listparindent=\parindent]

	\item {\bf Choice of discretization}

		As the system we want to solve is coupled, it is hard to use finite difference or finite volume to generate a stencil in space.Therefore, we choose finite element to discretize the problem. Moreover, as the equations involves evolution in time, we need to choose a time scheme as well. For this problem, I choose finite difference Crank-Nicholson scheme, for a second order global error in time.

	\item {\bf Choice of Linear/Non-linear Solver}

		For this coupled problem, the linear solver we choose is split field. And for non-linear solver, we choose newton for this problem.

	\item {\bf Choice of software for modeling/visualization/analysis}

		We choose TerraFERMA as the software for solving these problem, as it offers enough freedom for us to solve the problem, as well as getting data to analyze it. We use Paraview to view the result visually, and generate movies about the result. Python scripts are used for further data or convergence analysis.

	\item {\bf Weak form of the PDE's}

		In order to use TerraFERMA to solve these PDE's using finite element in space, and Crank-Nicholson in time, we need to formulate the weak form of these PDE's. They are shown as follows:
		\begin{align*}
			F_G = & \int_\Omega \big[G_t(G_{n+1} - G_\theta) + kG_t(\vec{v}_\theta\cdot\nabla G_\theta)\big] + D\nabla G_t\cdot\nabla G_\theta\big]\diff\Omega\\
			F_v = & \int_\Omega \big[\mu\nabla\vec{v}_{n+1}:\nabla\vec{v_t} - P_{n+1}\nabla\cdot\vec{v}_t + {\mathrm So} G_{n+1}\hat{y}\cdot\vec{v}_t\big]\diff\Omega\\
			F_P = & \int_\Omega P_t\nabla\cdot\vec{v}_{n+1}\diff\Omega
		\end{align*}
		
		Where,
		\begin{align*}
			G_\theta = & \theta G_{n+1} + (1-\theta)G_n\\
			\vec{v}_\theta = & \theta \vec{v}_{n+1} + (1-\theta)\vec{v}_n
		\end{align*}

		And $\theta=0.5$ is required for Crank-Nicholson scheme.

\end{enumerate}

\section{Results}

\begin{enumerate}[label=\arabic*., font=\bfseries, listparindent=\parindent]

	\item {\bf Result of Crank-Nicholson scheme}

		The final result of glucose distribution as well as the field of velocity is shown by the following figure. A movie could be found under the repository ``ShiroKoukutou/apma4301\_2013\_yihanzhang/Proje\\ct/CN.avi''. 
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.3]{./FigureForReport/CNfinal.eps}
			\caption{The glucose distribution and velocity field at time 200s}
		\end{figure}

		The distribution of glucose seems good, as we could see the effect of gravity on the diffusion, as well as water take glucose to the right side. Thus this result could reflect what is happening in the real device to some extent. However, It could be also seen that the concentration of glucose drops sharply near the entrance, and there is almost no glucose at the exit. Also, glucose distribute well under the membrane. This effect mainly because of the gravity term in the equation. A too small input velocity results in a large effect of gravity to the distribution of glucose, and very little of advection by water.

		One more thing to note in the result is that the velocity grows quickly in this simulation. This is not only because the large effect of gravity in this simulation, but also as a result of the free boundary at the right side. In order to form a better exit for water in the simulation, it is better to change the geometry of the problem.

		The convergence plot is shown as follows:
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./FigureForReport/CNcc2.png}
			\caption{Relative error in 2-Norm}
		\end{figure}
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./FigureForReport/CNccinf.png}
			\caption{Relative error in Inf-Norm}
		\end{figure}

		It could be seen that the relative error is small in both 2-Norm and Inf-Norm. Thus the numerical solution of the problem could be considered valid for the original set up.

		The mean glucose concentration at the bottom is shown as follows:
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./FigureForReport/CNgc.png}
			\caption{Mean glucose concentration vs. real time}
		\end{figure}

		From the plot, we could find that the concentration of the glucose at bottom of the device reaches $80\%$ of that in the entrance in no longer than 200 seconds. This result is still longer than that investigated in real case.

		Although the relative error is good for the solution, it is easy to find that the glucose concentration at entrance is not stable for the first few time steps. Therefore, it is not L-stable. As changing time step is way too time consuming, we could repeat the simulation using a backward Euler scheme to further investigate this stability issue.

	\item {\bf Result of backward Euler scheme}

		The solution using backward Euler scheme is very similar to that of Crank-Nicholson. However, the oscillation at the entrance vanished. Thus this scheme is a little bit better for the problem. The convergence plot, mean glucose concentration plot is shown as follows:
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./FigureForReport/BEcc2.png}
			\caption{Relative error in 2-Norm}
		\end{figure}
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./FigureForReport/BEccinf.png}
			\caption{Relative error in Inf-Norm}
		\end{figure}
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./FigureForReport/BEgc.png}
			\caption{Mean glucose concentration vs. real time}
		\end{figure}

		The relationship between mean glucose concentration and real time does not change between schemes. Thus further indicates the result is valid for given mathematical model of the device.

		The movie of the result of backward Euler scheme could also be found at ``ShiroKoukutou/apma43\\01\_2013\_yihanzhang/Project/BE.avi'' in the repository.

	\item {\bf Behavior for different ``So'' number}

		The intuition tells us that in real case, the movement of glucose molecule should be more dominated by the flow of water than gravity force. Thus the scale of number $\mathrm So$ in the equation becomes important. A solution generated with $\mathrm So = 10^{-2}$ using backward Euler scheme could be found at ``ShiroKoukutou/apma4301\_2013\_yihanzhang/Project/BESmallSo.avi''. In this movie, the time scale is magnitude larger than that used before. A result in half of total simulation time is shown in the following. We could see that this time, the glucose travels with water perfectly,and the effect of gravity and diffusion are also obvious at the time glucose travel through the membrane.

		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.4]{./FigureForReport/BEss.png}
			\caption{Simulation result of the problem with small $\mathrm So$ number}
		\end{figure}

		In compare with solutions before, this solution seems more real. Thus it indicates the possibility that there might be something wrong in the set up of the problem. As the expression of $\mathrm So$ is:
	$$
	\mathrm So = \frac{2\rho_0\beta G_0gh^2}{\mu_0v_0}
	$$
	
	One possibility is that the value of viscosity $\mu$ should not be considered as constant in this problem. Viscosity may increase as concentration of glucose goes up. Therefore, if the concentration is too high, the local $\mathrm So$ will drop down, and the time expected will be longer for a good diffusion in the bottom. Another possibility is that this $So$ number might be right, and that is similar to what happens in the experiment. However, for blood in a vessel, the viscosity may goes up in magnitude, thus making $\mathrm So$ goes up as well.

\end{enumerate}

\section{Further Discussion}

\begin{enumerate}[label=\arabic*., font=\bfseries, listparindent=\parindent]

	\item {\bf Traction forces on the wall}

		In the simulation above, we set the boundary condition of velocity field as no penetration. However, as a key factor of behavior of fluid, the traction forces on the wall may affect the result. In the real case, the surface of the device is not smooth either. This effect becomes more significant as the dimension of the real MEMS device goes down. As boundary layer theory shows, the velocity of the fluid is zero at the surface. If we set the Dirichlet boundary condition of velocity field into zero in both $x$ and $y$ components, the successful diffusion of glucose will be much longer (approximately ten times of the original one) with other configurations unchanged in the simulation. 
		
		This issue still require thorough investigation.

	\item {\bf Geometry}

		This is an issue partly discussed in previous section. As in current geometry, the boundary in the right of top region is free, and gives freedom to a very large velocity field that is far from real case. Thus one thing we need to do is change the geometry with small entrance area and small exit area. It will make the simulation more like the experiment as well. Also, if traction forces could be included at both entrance and exit channel, it will prevent the velocity field from going too high as well.

		Another thing to mention is that in real device, there are holes between the chamber under membrane, and sensor area. Further simulation should include that part of geometry as well.

	\item {\bf Viscosity}

		Also as discussed in the result section, the $\mathrm So$ number has large effect to the result. Small $\mathrm So$ number, which may comes from large viscosity, would generate a mostly uniform flow from left to right in the top region. Thus the viscosity used in the simulation should be further investigated.

	\item {\bf Model of the membrane}

		In the result of both schemes, we could find that the velocity at the bottom area is not small as we predicted when building up the mathematical model. This results in fluids vanished at the bottom surface of the membrane. However, treating the membrane as non-penetration boundary at both sides, as stated before, will make the pressure at bottom chamber goes up to infinity. This will make the problem unsolvable.

		A better mathematical model of this membrane in face of fluids may be an upper bound for the velocity of fluid penetrating it. However, it remains a problem that how to put this kind of constrain into PDE formation.

\end{enumerate}

\section{Conclusion and future work}

	This project uses TerraFERMA to solve a simplified version of physical problem numerically. The numerical method converges, and shows an valid result for the problem we want to solve. From the result, as well as the discussion above, it could be seen that there is evidence for us to believe the diffusion of glucose in the device is fast enough (in several minutes) for the sensor to measure. However, it still remains unknown how the result will be, if the viscosity becomes larger, and boundary traction forces are added in the simulation. Also, changes in geometry and model of membrane may also largely affect our conclusion. Therefore, future investigation is still required.

	The next step of the project is to change the geometry. And a simulation with concentration dependent viscosity might be done after that.

\end{document}
