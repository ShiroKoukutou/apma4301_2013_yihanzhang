# -*- coding: utf-8 -*-
"""
This is the python routine used for APMA4301 homework #2

Author: (Fenny) Yihan Zhang
Last revised: 26th Sep. 2013
"""

import numpy
import pylab
import diffMatrix_revised as dm

# Define the number of point in mesh
base = 2
deg = range(3,10)
N = [base ** i for i in deg]

# Define h
h = [1.0/i for i in N]

# Initialize eh1 and eh2
eh1 = pylab.zeros(len(N))
eh2 = pylab.zeros(len(N))

# Loop to find e_h's for each N
for n in range(len(N)):
    
    # Define mesh
    x = numpy.linspace(0,1,N[n]+1)
    
    # Define f(x) and its derivatives
    fx = x**2 + numpy.sin(4 * numpy.pi * x)
    dfx = 2 * x + 4 * numpy.pi * numpy.cos(4 * numpy.pi * x)
    d2fx = 2 - 16 * (numpy.pi ** 2) * numpy.sin(4 * numpy.pi * x)
    
    # Calculate D
    D1 = dm.setD(1,x)
    D2 = dm.setD(2,x)
    
    # Calculate eh1 and eh2
    eh1[n] = numpy.sum([i ** 2 for i in (D1*fx - dfx)]) ** 0.5\
            * numpy.sqrt(h[n])
    eh2[n] = numpy.sum([i ** 2 for i in (D2*fx - d2fx)]) ** 0.5\
            * numpy.sqrt(h[n])
    
# Calculate the logarimatic value of error and h
log10h = numpy.log10(h)
log10eh1 = numpy.log10(eh1)
log10eh2 = numpy.log10(eh2)

# Plot the figures of log_10(error)
pylab.figure
pylab.hold(True)
pylab.plot(log10h,log10eh1,label=r'$Error\ of\ first\ derivatives\ (k=1)$')
pylab.plot(log10h,log10eh2,label=r'$Error\ of\ second\ derivatives\ (k=2)$')
pylab.legend(loc = 'lower right')
pylab.title("The log-log plot of error $e_h$ against $h$")
pylab.xlabel("$log_{10}(h)$")
pylab.ylabel("$log_{10}(e_h)$")
pylab.hold(False)

# Polynomial fitting the log-log plots:
peh1 = numpy.polyfit(log10h,log10eh1,1)
peh2 = numpy.polyfit(log10h,log10eh2,1)

# Print the value of p
print peh1[0]
print peh2[0]