# -*- coding: utf-8 -*-
"""
This is the python routine used for APMA4301 homework #2

Author: (Fenny) Yihan Zhang
Last revised: 25th Sep. 2013
"""

import numpy
import diffMatrix as dm

# Define the number of point in mesh
N = [8,16,32,64,128,256,512,1024]

