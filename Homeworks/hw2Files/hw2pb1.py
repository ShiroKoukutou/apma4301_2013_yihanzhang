# -*- coding: utf-8 -*-
"""
This is the python routine used for APMA4301 homework #2

Author: (Fenny) Yihan Zhang
Last revised: 25th Sep. 2013
"""

import fdcoeffV
import numpy

# Set the value of h
h = 0.2

# Generate the original mesh
x = numpy.array([0,1,2])*h

# Calculate the stencils for first derivatives
c10 = fdcoeffV.fdcoeffV(1,0,x)
c11 = fdcoeffV.fdcoeffV(1,0.1,x)
c12 = fdcoeffV.fdcoeffV(1,0.2,x)

# Display the results
print "C_1_0: " + str(c10)
print "C_1_05: " + str(c11)
print "C_1_1: " + str(c12)

# Repeat for second derivatives
c20 = fdcoeffV.fdcoeffV(2,0,x)
c21 = fdcoeffV.fdcoeffV(2,0.1,x)
c22 = fdcoeffV.fdcoeffV(2,0.2,x)
print "C_1_0: " + str(c20)
print "C_1_05: " + str(c21)
print "C_1_1: " + str(c22)