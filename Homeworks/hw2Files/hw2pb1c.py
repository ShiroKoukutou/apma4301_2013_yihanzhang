# -*- coding: utf-8 -*-
"""
This is the python routine used for APMA4301 homework #2

Author: (Fenny) Yihan Zhang
Last revised: 26th Sep. 2013
"""

from sympy import *

# Define mesh point x0, x1, x2 and solving point xbar
x0, x1, x2, xbar = symbols('x0 x1 x2 xbar')
deltax0 = xbar - x0
deltax1 = xbar - x1
deltax2 = xbar - x2

# Define h
h = symbols('h')

# Define the matrix related to Taylor's series
M = Matrix([[1,1,1],[deltax0,deltax1,deltax2],\
            [deltax0**2/2,deltax1**2/2,deltax2**2/2]])
# Calculate the inverse of it
Minv = M.inv()
            
# Define the stencil vector
c11, c12, c13, c21, c22, c23 = symbols('c11 c12 c13 c21 c22 c23')
C1 = Matrix([[c11],[c12],[c13]])
C2 = Matrix([[c21],[c22],[c23]])

# Define the derivative vector related to first and second derivative
k1 = Matrix([[0],[1],[0]])
k2 = Matrix([[0],[0],[1]])

# Calculate the vector C
C1 = Minv*k1
C2 = Minv*k2

# Substitude and display the results
C1sub = C1.subs([(x0, 0), (x1, h), (x2, 2*h)])
C2sub = C2.subs([(x0, 0), (x1, h), (x2, 2*h)])
print 'Stencils for first derivative at xbar = 0h:  '\
        + str(C1sub.subs(xbar,0))
print 'Stencils for first derivative at xbar = 0.5h:  '\
        + str(C1sub.subs(xbar,0.5 * h))
print 'Stencils for first derivative at xbar = 1h:  '\
        + str(C1sub.subs(xbar,h))
print 'Stencils for second derivative at xbar = 0h:  '\
        + str(C2sub.subs(xbar,0))
print 'Stencils for second derivative at xbar = 0.5h:  '\
        + str(C2sub.subs(xbar,0.5*h))
print 'Stencils for second derivative at xbar = 1h:  '\
        + str(C2sub.subs(xbar,h))