% English Version of fe-i-tex

\documentclass{article}

% Set margins:
\hoffset = -0.9in
\voffset = -0.75in
\textheight = 680pt
\textwidth = 470pt

% Mathematic libraries
\usepackage{amsmath}
\usepackage{amssymb}
\newcommand{\diff}{\mathrm{d}}
\newcommand{\me}{\mathrm{e}}
\newcommand{\mi}{\mathrm{i}}
\newcommand{\erf}{\mathrm{erf}}

% Symbol libraries
\usepackage{textcomp}

% Code display library
\usepackage{mylistings}

% Graphic libraries
\usepackage{float}
\usepackage{graphicx}

% Enumerate label libraries
\usepackage{enumerate}

% Font package
\usepackage{times}

\usepackage{setspace}

% TODO: Add your article here.

\begin{document}

\title{Numerical Method for Partial Differential Equations:\\ Solution to Homework \#3}
\author{Yihan Zhang (Uni: yz2567)}
\maketitle

\begin{enumerate}[\bfseries \large Problem 1.]
  \item Poisson Equation in 1-D  
    \begin{enumerate}[\hspace{-0.5in} \bf {Part} (a)]
      \item {\bf Solution}: Assuming:
	$$
	u = C_1x^2 + C_2x + C_3
	$$
	Thus from the equation, we can find:
	$$
	-2C_1 = 1 \qquad \Rightarrow \qquad C_1 = -\frac{1}{2}
	$$ 
	Plug in the boundary conditions, we can therefore determine the value of $C_2$ and $C_3$:
	\begin{align*}
	  C_3 & = 0\\
	  C_1 + C_2 + C_3 & = 1 \qquad \Rightarrow \qquad C_2 = \frac{3}{2}
	\end{align*}
	Therefore, an analytical solution for the 1-D Poisson equation with boundary conditions is:
	$$
	u = -\frac{1}{2} x^2 + \frac{3}{2} x 
	$$

      \item {\bf Solution}: After modifying the python program, we can find the convergence plot of error against grid spacing $h$ as follows:
	\begin{figure}[H]
	  \centering
	  \includegraphics[scale=0.5]{./PythonFiles/hw3pb1.png}
	  \caption{Error against $h$}
	\end{figure}

	The python routine is shown as follows:
	\lstinputlisting[escapechar='', style=customPython]{./PythonFiles/hw3pb1.py}
      \item {\bf Solution}: From the plot, we can see that the convergence rate is a negative number, which seems to mean that the problem does not converge. 
	However, the manufactured solution is a second order polynomial, which means it's third and higher order derivatives are all zero. Thus the error terms caused by approximating the function using finite neighbour points and finite order of polynomial should all be zero. 
	If we look at distribution of the error (which is shown as follows), we can find that it is highest in the middle, however zero at edges, which also behaves differently in compare with prediction. Another thing to mention is that it is much smaller than truncation errors before, and the magnitude is much similar to that of the machine precision. Therefore, this error-spacing relation should have nothing to do with the convergence, however, may be caused by the algorithm and the inaccuracy in floating numbers and operations related to them.

	\begin{figure}[H]
	  \centering
	  \includegraphics[scale = 0.5]{./PythonFiles/hw3pb1c.png}
	  \caption{The distribution of error along x-axis}
	\end{figure}
      
    \end{enumerate}

  \item Poisson Equation in 2-D 
    \begin{enumerate}[\hspace{-0.5in} \bf {Part} (a)]
      \item Stability
	\begin{enumerate}[\hspace{-0.3in} (i)]
	  \item {\bf Solution}: For the assumed eigenfunctions, apply the interior stencil will yield to:
	    \begin{align*}
	      \nabla^2_5 \phi_{mn} = & \frac{1}{h^2}
	      \begin{bmatrix}
		& 1 & \\
		1 & -4 & 1\\
		& 1 &
	      \end{bmatrix}
	      \sin (m\pi x) \sin (n\pi y)\\
	      = & \frac{1}{h^2} \left[ \sin \big(m\pi h (i-1)\big) \sin \big(n\pi hj\big) + \sin \big(m\pi hi\big) \sin \big(n\pi h (j-1)\big) \right. \\
	      & + \sin \big(m\pi h (i+1)\big) \sin \big(n\pi hj\big) + \sin \big(m\pi hi\big) \sin \big(n\pi h(j+1)\big)\\
	      & \left. - 4\sin \big(m\pi h i\big) \sin \big(n\pi hj\big)\right]\\
	      = & \frac{1}{h^2} \big(2\cos(m\pi h) + 2\cos(n\pi h) - 4\big) \sin(m\pi hi)\sin(n\pi hj)\\
	      = & \lambda_{mn} \sin(m\pi x)\sin(n\pi y)
	    \end{align*}
	    Where in the equation:
	    \begin{align*}
	      \lambda_{mn} = & \frac{2}{h^2}\big(\cos(m\pi h) + cos(n\pi h) - 2\big)\\
	      \approx & \frac{2}{h^2}\big( (1-\frac{(m\pi h)^2}{2}) +(1-\frac{(n\pi h)^2}{2}) -2 \big)\\
	      = & -(m^2+n^2)\pi^2 , \qquad when \,\, h\rightarrow 0
	    \end{align*}
	  \item {\bf Solution}: Considering the 2-Norm of the inverse of a matrix equals to:
	    $$
	    ||A^{-1}|| = max(|\frac{1}{\lambda_{mn}}|)
	    $$
	    For the interier area, where the boundary is always consider as homogenious, we can find that:
	    \begin{align*}
	      m = [1,\frac{1}{h}] \cap \mathbb{Z}\\
	      n = [1,\frac{1}{h}] \cap \mathbb{Z}
	    \end{align*}
	    Therefore:
	    $$
	    C = \lim_{h\rightarrow 0}||A^{-1}|| = max(\lim_{h\rightarrow 0}|\frac{1}{\lambda_{mn}}|) = max(\frac{1}{(m^2+n^2)\pi^2}) = \frac{1}{2\pi^2}
	    $$
	\end{enumerate}
      \item {\bf Solution}: After modifying the python file poisson2d.py, we can use this to derive a relationship between the precise of mesh $h$ and the error using the manufactured solution:
	$$
	u(x,y) = e^{x+y/2}
	$$
	The log-log plot for error against $h$ is as following:
	\begin{figure}[H]
	  \centering
	  \includegraphics[scale = 0.5]{./PythonFiles/hw3pb2b.png}
	  \caption{Error against $h$}
	\end{figure}
	Where in the plot, we can find the convergence rate approximately equals to:
	$$
	p=2.00
	$$
	For analytical analysis, we can find that in two dimensional poisson equation, the stencils will lead to an error approximately proportional to $h^2$. Thus for two dimensional problem:
	$$
	r = \sqrt{h_xh_y}\sqrt{N_xN_y(h^2)^2} = h\sqrt{h^{-2}h^4} = h^2
	$$
	Which is perfectly the same as the result of previous experiment.

	The python program is shown as follows:
	\lstinputlisting[escapechar='', style=customPython]{./PythonFiles/hw3pb2b.py}
      \item Themes and Variations 
	\begin{enumerate}[\hspace{-0.3in} (i)]
	  \item {\bf Solution}: Modify the python routine, we can then derive a log-log plot between error and $h$ on a non-square domain:
	    \begin{figure}[H]
	      \centering
	      \includegraphics[scale = 0.5]{./PythonFiles/hw3pb2c1.png}
	      \caption{Error against spacing on a non-square domain}
	    \end{figure}

	    A change in shape of the domain should not make any difference in convergence. And the experimental result proves that. The modified parts of the routine in compare with that of {\bf Part (b)} is shown as follows:
 	    \lstinputlisting[escapechar='', style=customPython, firstline=28, lastline=37]{./PythonFiles/hw3pb2ci.py}
 	    \lstinputlisting[escapechar='', style=customPython, firstline=62, lastline=79]{./PythonFiles/hw3pb2ci.py}
 	    \lstinputlisting[escapechar='', style=customPython, firstline=88, lastline=90]{./PythonFiles/hw3pb2ci.py}

	  \item{\bf Solution}: Further modify the python routine, we can derive a log-log plot between error and $h$ on a mesh where $\Delta x \neq \Delta y$
	    \begin{figure}[H]
	      \centering
	      \includegraphics[scale = 0.5]{./PythonFiles/hw3pb2c2.png}
	      \caption{Error against spacing when $\Delta x \neq \Delta y$}
	    \end{figure}

	    A change in the spacing should not change the convergence rate as well. And the experimantal result also proves that. The modified parts of the routine in compare with that of {\bf Part (b)} is shown as follows:

 	    \lstinputlisting[escapechar='', style=customPython, firstline=28, lastline=37]{./PythonFiles/hw3pb2cii.py}
 	    \lstinputlisting[escapechar='', style=customPython, firstline=61, lastline=76]{./PythonFiles/hw3pb2cii.py}

	\end{enumerate}
    \end{enumerate}
    
\end{enumerate}

\end{document}
