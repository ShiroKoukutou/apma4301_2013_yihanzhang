# -*- coding: utf-8 -*-
"""
poisson2d_mms.py:  

@author: Tian Cao
"""
from __future__ import division
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import numpy as np
import pylab

from diffMatrix import setD

#def u_exact(x):
#    """ 
#    exact solution for this problem
#    sin(3*pi*x)+x
#    """
#    # return exact solution
#    return np.sin(3.*np.pi*x) + x

def u_exact_matrix(x,y):
    """ 
    exact solution for this problem
    exp(x+y/2)
    """
    def u_exact(x, y):
        return np.exp(x+(y/2.))
   
    M = len(x)
    N = len(y)
    u_matrix = np.zeros(shape=(M,N))

    for i in xrange(0,N):
        u_matrix[:,i] = u_exact(x,y[i])
        
    return u_matrix
    
#def f_exact(x):
#    """
#    return rhs for manufactured solution given by u_exact
#    """
#    return (3.*np.pi)**2*np.sin(3.*np.pi*x)
    
def f_exact_matrix(x,y):
    """
    return rhs for manufactured solution given by u_exact
    """
    def f_exact(x,y):
        return (5./4.)*np.exp(x+(y/2.));
    
    M = len(x)
    N = len(y)
    
    f_matrix = np.zeros(shape=(M,N))

    for i in xrange(1,N-1):
        f_matrix[:,i] = f_exact(x,y[i])
        
    return f_matrix[1:-1,1:-1]
    
def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """
    
    return np.sqrt(h)*np.linalg.norm(f, 2)
    
## plot solution and errors
#def plotpoisson1d(x,u_solve,title=None):
#    """ Quick test routine to display solution of 
#        input: x: numpy array of mesh-points
#               f: function pointer to function to differentiate
#    """  
#
#    # compare solution to analytic
#    pylab.figure()
#    pylab.plot(x,u_solve,'rx-',x,u_exact(x),'b+-')
#    pylab.title("compare solutions N={0}".format(len(x)))
#    pylab.legend(["u_h", "u_exact"],loc="best")
#    pylab.xlabel("x")
#    pylab.ylabel("u")
#    pylab.grid()
#        
#    # plot errors
#    err = u_solve - u_exact(x)    
#    pylab.figure()
#    pylab.plot(x,err)
#    pylab.xlabel("x")
#    pylab.ylabel("err")
#    pylab.title("errors, N={0}".format(len(x)))
#    if title:
#        pylab.title(title)
#    pylab.show()

def plotconvergence(N_ar,abs_err_ar,rel_err_ar):
    """ make pretty convergence plots
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    h = 1./np.array(N_ar)
    abs_err = np.array(abs_err_ar)
    rel_err = np.array(rel_err_ar)  
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    p = np.polyfit(np.log(h),np.log(abs_err),1)
    print 'p=', p
    
    pylab.figure()
    pylab.loglog(h,abs_err,'bo-',h,rel_err,'ro-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.3}".format(p[0]))
    pylab.legend(["abs_err","rel_error","best-fit p"],loc="best")
    pylab.grid()
    pylab.show()
    

def main(a,b):
    # set number of mesh intervals (mesh points is N+1)
    N_ar = [16,32,64,128,256]
    #N_ar = [16]
    # initialize lists for storing output
    abs_err_ar = []
    rel_err_ar = []
    for N in N_ar:
        
        h_x = (a*1.0)/(N+1)
        h_y = (b*1.0)/(N+1)
        
        x = np.linspace(0,a,N+2)
        y = np.linspace(0,b,N+2)       
        m = len(x)-2 #number of interior points of x
        n = len(y)-2 #number of interior points of y                
        
        #get the exact function
        u_true = u_exact_matrix(x,y)
        #duplicate u_true for u_soln matrix
        u_soln = u_true.copy()
        u_soln[1:-1,1:-1].fill(0) #fill the interior points of solution as 0
        
        alpha = h_x/h_y
        alpha_inv = 1./alpha        
        
        # form matrix A:
        ttlSize = m*n #number of interior points
        A = sp.lil_matrix((ttlSize,ttlSize))
        for i in range(0,m):
            for j in range(0,n):
                k = i*n+j
                A[k,k] = -2.*(alpha+alpha_inv)
                if j != 0:
                    A[k,k-1] = alpha_inv
                if j != (n-1):
                    A[k,k+1] = alpha_inv
                if i != 0:
                    A[k,k-n] = alpha
                if i != (m-1):
                    A[k,k+n] = alpha  
        A = A/(h_x*h_y)
        A = A.tocsr()
        
        # set rhs vector
        rhs = f_exact_matrix(x,y)
        # adjust the rhs to include boundary terms: 
        rhs[:,0] -= u_soln[1:-1,0] / (h_x*h_y)
        rhs[:,-1] -= u_soln[1:-1,-1] / (h_x*h_y)
        rhs[0,:] -= u_soln[0,1:-1] / (h_x*h_y)
        rhs[-1,:] -= u_soln[-1,1:-1] / (h_x*h_y)        
        # convert the 2d grid function rhs into a column vector for rhs of system:
        F = rhs.reshape((m*n,1))
    
        # Solve the linear system:
        uvec = spsolve(A, F)

        # reshape vector solution uvec as a grid function and
        # insert this interior solution into usoln for plotting purposes:
        # (recall boundary conditions in usoln are already set)       
        u_soln[1:-1, 1:-1] = uvec.reshape( (m,n) )

        # calculate error and errornorm        
        err = u_soln - u_true
        #print 'err = ', err
        abs_err = grid_norm2(err,np.sqrt((h_x*h_y)))
        rel_err = abs_err/grid_norm2(u_soln,np.sqrt((h_x*h_y)))
        print 'N=', N, 'abs_err=', abs_err, 'rel_err=',rel_err
        
        # collect results for later plotting 
        abs_err_ar.append(abs_err)
        rel_err_ar.append(rel_err)
        
#    # plot it out
#    plotpoisson1d(x,u_solve)
    plotconvergence(N_ar,abs_err_ar,rel_err_ar)
    

if __name__ == "__main__":
    main(2,1)
