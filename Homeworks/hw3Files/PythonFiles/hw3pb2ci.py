"""
poisson.py  -- solve the Poisson problem u_{xx} + u_{yy} = f(x,y)
                on [a,b] x [a,b].
                
                using python sparse linear algebra

     The 5-point Laplacian is used at interior grid points.
     This system of equations is then solved using scipy.sparse.linalg spsolve
     which defaults to umfpack

     code modified from poisson.py from 
     http://www.amath.washington.edu/~rjl/fdmbook/chapter3  (2007)
     
Modified by Yihan Zhang
Last version on Oct 8th, 2013
"""

import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pylab
from mpl_toolkits.mplot3d import Axes3D

Mrange = range(4,9)
error = np.zeros(len(Mrange))

for i in range(len(Mrange)):   
    ax = 0.0
    bx = 2.0
    ay = 0.0
    by = 1.0
    m = 2.**Mrange[i]           # number of interior points in each direction
    hy = (by-ay)/(m+1)
    hx = hy
    alpha = hx/hy
    x = np.linspace(ax,bx,2*m+3)   # grid points x including boundaries
    y = np.linspace(ay,by,m+2)   # grid points y including boundaries
    
    
    X,Y = np.meshgrid(x,y)     # 2d arrays of x,y values
    X = X.T                    # transpose so that X(i,j),Y(i,j) are
    Y = Y.T                    # coordinates of (i,j) point
    
    Xint = X[1:-1,1:-1]        # interior points
    Yint = Y[1:-1,1:-1]
    
    def f(x,y):
        return 5./4*np.exp(x+y/2.)
    
    rhs = f(Xint,Yint)         # evaluate f at interior points for right hand side
                               # rhs is modified below for boundary conditions.
    
    # set boundary conditions around edges of usoln array:
    
    usoln = np.exp(X+Y/2.)     # here we just zero everything  
                               # This sets full array, but only boundary values
                               # are used below.  For a problem where utrue
                               # is not known, would have to set each edge of
                               # usoln to the desired Dirichlet boundary values.
    
    
    # adjust the rhs to include boundary terms: 
    rhs[:,0] -= usoln[1:-1,0] / hx**2
    rhs[:,-1] -= usoln[1:-1,-1] / hx**2
    rhs[0,:] -= usoln[0,1:-1] / hy**2
    rhs[-1,:] -= usoln[-1,1:-1] / hy**2
    
    # convert the 2d grid function rhs into a column vector for rhs of system:
    F = rhs.reshape((m*(2*m+1),1))
    
    # form matrix A:
    Im = sp.eye(m,m)
    I2m = sp.eye(2*m+1,2*m+1)
    em = np.ones(m)
    e2m = np.ones(2*m+1)
    T = sp.spdiags([em/alpha,-2.*em*(1./alpha+alpha),em/alpha],[-1,0,1],m,m)
    S = sp.spdiags([e2m*alpha,e2m*alpha],[-1,1],2*m+1,2*m+1)
    A = (sp.kron(I2m,T) + sp.kron(S,Im)) / (hx*hy)
    A = A.tocsr()
    
    # Solve the linear system:
    uvec = spsolve(A, F)
    
    # reshape vector solution uvec as a grid function and
    # insert this interior solution into usoln for plotting purposes:
    # (recall boundary conditions in usoln are already set)
    
    udiff = np.zeros(usoln.shape)
    udiff[1:-1, 1:-1] = uvec.reshape( (2*m+1,m) ) - usoln[1:-1, 1:-1]
    usoln[1:-1, 1:-1] = uvec.reshape( (2*m+1,m) )
    
    # using Linf norm of spectral solution good to 10 significant digits
    error[i] = np.sqrt(hx*hy)*np.linalg.norm(udiff,2)
    print "m = {0}".format(m)
    print "2-Norm = {0}".format(error[i])

show_matrix = False
if (show_matrix):
    pylab.spy(sp.kron(S,Im),marker='.')

show_result = False
if show_result:
# plot results:
    pylab.figure()
    ax = Axes3D(pylab.gcf())
    ax.plot_surface(X,Y,rhs, rstride=1, cstride=1, cmap=pylab.cm.jet)
    ax.set_xlabel('t')
    ax.set_ylabel('x')
    ax.set_zlabel('u')
    #pylab.axis([a, b, a, b])
    #pylab.daspect([1 1 1])
    pylab.title('Surface plot of computed solution')

pylab.show(block=False)

logH = np.log10([1./(2**i+1) for i in Mrange])
logE = np.log10(error)
p = np.polyfit(logH,logE,1)

pylab.figure()
pylab.plot(logH,logE)
pylab.title("Error against h (p={0})".format(p[0]))
pylab.xlabel("$log_{10}h$")
pylab.ylabel("$log_{10}Error$")
