# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/fenny/Work/TerraFERMA/share/terraferma/cpp

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/fenny/documents/NumericalMethodsOfPDE/GitBucket/Homeworks/hw4/pb2bi/build_convergence

# Include any dependencies generated for this target.
include CMakeFiles/poisson_convergence.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/poisson_convergence.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/poisson_convergence.dir/flags.make

CMakeFiles/poisson_convergence.dir/main.cpp.o: CMakeFiles/poisson_convergence.dir/flags.make
CMakeFiles/poisson_convergence.dir/main.cpp.o: /home/fenny/Work/TerraFERMA/share/terraferma/cpp/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/fenny/documents/NumericalMethodsOfPDE/GitBucket/Homeworks/hw4/pb2bi/build_convergence/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/poisson_convergence.dir/main.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/poisson_convergence.dir/main.cpp.o -c /home/fenny/Work/TerraFERMA/share/terraferma/cpp/main.cpp

CMakeFiles/poisson_convergence.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/poisson_convergence.dir/main.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/fenny/Work/TerraFERMA/share/terraferma/cpp/main.cpp > CMakeFiles/poisson_convergence.dir/main.cpp.i

CMakeFiles/poisson_convergence.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/poisson_convergence.dir/main.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/fenny/Work/TerraFERMA/share/terraferma/cpp/main.cpp -o CMakeFiles/poisson_convergence.dir/main.cpp.s

CMakeFiles/poisson_convergence.dir/main.cpp.o.requires:
.PHONY : CMakeFiles/poisson_convergence.dir/main.cpp.o.requires

CMakeFiles/poisson_convergence.dir/main.cpp.o.provides: CMakeFiles/poisson_convergence.dir/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/poisson_convergence.dir/build.make CMakeFiles/poisson_convergence.dir/main.cpp.o.provides.build
.PHONY : CMakeFiles/poisson_convergence.dir/main.cpp.o.provides

CMakeFiles/poisson_convergence.dir/main.cpp.o.provides.build: CMakeFiles/poisson_convergence.dir/main.cpp.o

# Object files for target poisson_convergence
poisson_convergence_OBJECTS = \
"CMakeFiles/poisson_convergence.dir/main.cpp.o"

# External object files for target poisson_convergence
poisson_convergence_EXTERNAL_OBJECTS =

poisson_convergence: CMakeFiles/poisson_convergence.dir/main.cpp.o
poisson_convergence: CMakeFiles/poisson_convergence.dir/build.make
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libdolfin.so
poisson_convergence: /usr/lib/i386-linux-gnu/libxml2.so
poisson_convergence: /usr/lib/libarmadillo.so
poisson_convergence: /usr/lib/liblapack.so
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/libboost_filesystem-mt.so
poisson_convergence: /usr/lib/libboost_program_options-mt.so
poisson_convergence: /usr/lib/libboost_system-mt.so
poisson_convergence: /usr/lib/libboost_thread-mt.so
poisson_convergence: /usr/lib/libboost_iostreams-mt.so
poisson_convergence: /usr/lib/libboost_mpi-mt.so
poisson_convergence: /usr/lib/libboost_serialization-mt.so
poisson_convergence: /usr/lib/libboost_timer-mt.so
poisson_convergence: /usr/lib/libboost_chrono-mt.so
poisson_convergence: /usr/lib/libhdf5.so
poisson_convergence: /usr/lib/i386-linux-gnu/libpthread.so
poisson_convergence: /usr/lib/i386-linux-gnu/libz.so
poisson_convergence: /usr/lib/i386-linux-gnu/librt.so
poisson_convergence: /usr/lib/i386-linux-gnu/libm.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libpetsc.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libumfpack.a
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/libcholmod.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
poisson_convergence: /usr/lib/libcamd.so
poisson_convergence: /usr/lib/libcolamd.so
poisson_convergence: /usr/lib/libccolamd.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libparmetis.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libmetis.so
poisson_convergence: /usr/lib/liblapack.so
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/gcc/i686-linux-gnu/4.7/libgfortran.so
poisson_convergence: /usr/lib/gcc/i686-linux-gnu/4.7/libgfortran.so
poisson_convergence: /usr/lib/libptscotch.so
poisson_convergence: /usr/lib/libptesmumps.so
poisson_convergence: /usr/lib/libptscotcherr.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libparmetis.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libmetis.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libCGAL.so
poisson_convergence: /usr/lib/libboost_thread-mt.so
poisson_convergence: /usr/lib/libboost_system-mt.so
poisson_convergence: /usr/lib/i386-linux-gnu/libgmp.so
poisson_convergence: /usr/lib/i386-linux-gnu/libmpfr.so
poisson_convergence: /usr/lib/i386-linux-gnu/libz.so
poisson_convergence: /usr/lib/libcppunit.so
poisson_convergence: /usr/lib/openmpi/lib/libmpi_cxx.so
poisson_convergence: /usr/lib/openmpi/lib/libmpi.so
poisson_convergence: /usr/lib/openmpi/lib/libopen-rte.so
poisson_convergence: /usr/lib/openmpi/lib/libopen-pal.so
poisson_convergence: /usr/lib/i386-linux-gnu/libdl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libnsl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libutil.so
poisson_convergence: /usr/lib/i386-linux-gnu/libm.so
poisson_convergence: /usr/lib/i386-linux-gnu/libdl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libQtGui.so
poisson_convergence: /usr/lib/i386-linux-gnu/libQtCore.so
poisson_convergence: /usr/lib/libvtkCommon.so
poisson_convergence: /usr/lib/libvtkFiltering.so
poisson_convergence: /usr/lib/libvtkImaging.so
poisson_convergence: /usr/lib/libvtkGraphics.so
poisson_convergence: /usr/lib/libvtkGenericFiltering.so
poisson_convergence: /usr/lib/libvtkIO.so
poisson_convergence: /usr/lib/libvtkRendering.so
poisson_convergence: /usr/lib/libvtkVolumeRendering.so
poisson_convergence: /usr/lib/libvtkHybrid.so
poisson_convergence: /usr/lib/libvtkWidgets.so
poisson_convergence: /usr/lib/libvtkParallel.so
poisson_convergence: /usr/lib/libvtkInfovis.so
poisson_convergence: /usr/lib/libvtkGeovis.so
poisson_convergence: /usr/lib/libvtkViews.so
poisson_convergence: /usr/lib/libvtkCharts.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libbuckettools_cpp.so
poisson_convergence: buckettools_ufc/libbuckettools_ufc.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libspud.so
poisson_convergence: /usr/lib/i386-linux-gnu/libpython2.7.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libdolfin.so
poisson_convergence: /usr/lib/i386-linux-gnu/libxml2.so
poisson_convergence: /usr/lib/libarmadillo.so
poisson_convergence: /usr/lib/liblapack.so
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/libboost_filesystem-mt.so
poisson_convergence: /usr/lib/libboost_program_options-mt.so
poisson_convergence: /usr/lib/libboost_system-mt.so
poisson_convergence: /usr/lib/libboost_thread-mt.so
poisson_convergence: /usr/lib/libboost_iostreams-mt.so
poisson_convergence: /usr/lib/libboost_mpi-mt.so
poisson_convergence: /usr/lib/libboost_serialization-mt.so
poisson_convergence: /usr/lib/libboost_timer-mt.so
poisson_convergence: /usr/lib/libboost_chrono-mt.so
poisson_convergence: /usr/lib/libhdf5.so
poisson_convergence: /usr/lib/i386-linux-gnu/libpthread.so
poisson_convergence: /usr/lib/i386-linux-gnu/libz.so
poisson_convergence: /usr/lib/i386-linux-gnu/librt.so
poisson_convergence: /usr/lib/i386-linux-gnu/libm.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libpetsc.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libumfpack.a
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
poisson_convergence: /usr/lib/libcholmod.so
poisson_convergence: /usr/lib/libcamd.so
poisson_convergence: /usr/lib/libcolamd.so
poisson_convergence: /usr/lib/libccolamd.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libparmetis.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libmetis.so
poisson_convergence: /usr/lib/gcc/i686-linux-gnu/4.7/libgfortran.so
poisson_convergence: /usr/lib/libptscotch.so
poisson_convergence: /usr/lib/libptesmumps.so
poisson_convergence: /usr/lib/libptscotcherr.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libCGAL.so
poisson_convergence: /usr/lib/i386-linux-gnu/libgmp.so
poisson_convergence: /usr/lib/i386-linux-gnu/libmpfr.so
poisson_convergence: /usr/lib/libcppunit.so
poisson_convergence: /usr/lib/openmpi/lib/libmpi_cxx.so
poisson_convergence: /usr/lib/openmpi/lib/libmpi.so
poisson_convergence: /usr/lib/openmpi/lib/libopen-rte.so
poisson_convergence: /usr/lib/openmpi/lib/libopen-pal.so
poisson_convergence: /usr/lib/i386-linux-gnu/libdl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libnsl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libutil.so
poisson_convergence: /usr/lib/liblapack.so
poisson_convergence: /usr/lib/libcblas.so
poisson_convergence: /usr/lib/libf77blas.so
poisson_convergence: /usr/lib/libatlas.so
poisson_convergence: /usr/lib/libboost_filesystem-mt.so
poisson_convergence: /usr/lib/libboost_program_options-mt.so
poisson_convergence: /usr/lib/libboost_system-mt.so
poisson_convergence: /usr/lib/libboost_thread-mt.so
poisson_convergence: /usr/lib/libboost_iostreams-mt.so
poisson_convergence: /usr/lib/libboost_mpi-mt.so
poisson_convergence: /usr/lib/libboost_serialization-mt.so
poisson_convergence: /usr/lib/libboost_timer-mt.so
poisson_convergence: /usr/lib/libboost_chrono-mt.so
poisson_convergence: /usr/lib/libhdf5.so
poisson_convergence: /usr/lib/i386-linux-gnu/libpthread.so
poisson_convergence: /usr/lib/i386-linux-gnu/libz.so
poisson_convergence: /usr/lib/i386-linux-gnu/librt.so
poisson_convergence: /usr/lib/i386-linux-gnu/libm.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libpetsc.so
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libumfpack.a
poisson_convergence: /home/fenny/Work/TerraFERMA/src/petsc/linux-gnu-cxx-opt/lib/libamd.a
poisson_convergence: /usr/lib/libcholmod.so
poisson_convergence: /usr/lib/libcamd.so
poisson_convergence: /usr/lib/libcolamd.so
poisson_convergence: /usr/lib/libccolamd.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libparmetis.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libmetis.so
poisson_convergence: /usr/lib/gcc/i686-linux-gnu/4.7/libgfortran.so
poisson_convergence: /usr/lib/libptscotch.so
poisson_convergence: /usr/lib/libptesmumps.so
poisson_convergence: /usr/lib/libptscotcherr.so
poisson_convergence: /home/fenny/Work/TerraFERMA/lib/libCGAL.so
poisson_convergence: /usr/lib/i386-linux-gnu/libgmp.so
poisson_convergence: /usr/lib/i386-linux-gnu/libmpfr.so
poisson_convergence: /usr/lib/libcppunit.so
poisson_convergence: /usr/lib/openmpi/lib/libmpi_cxx.so
poisson_convergence: /usr/lib/openmpi/lib/libmpi.so
poisson_convergence: /usr/lib/openmpi/lib/libopen-rte.so
poisson_convergence: /usr/lib/openmpi/lib/libopen-pal.so
poisson_convergence: /usr/lib/i386-linux-gnu/libdl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libnsl.so
poisson_convergence: /usr/lib/i386-linux-gnu/libutil.so
poisson_convergence: /usr/lib/i386-linux-gnu/libQtGui.so
poisson_convergence: /usr/lib/i386-linux-gnu/libQtCore.so
poisson_convergence: /usr/lib/libvtkCommon.so
poisson_convergence: /usr/lib/libvtkFiltering.so
poisson_convergence: /usr/lib/libvtkImaging.so
poisson_convergence: /usr/lib/libvtkGraphics.so
poisson_convergence: /usr/lib/libvtkGenericFiltering.so
poisson_convergence: /usr/lib/libvtkIO.so
poisson_convergence: /usr/lib/libvtkRendering.so
poisson_convergence: /usr/lib/libvtkVolumeRendering.so
poisson_convergence: /usr/lib/libvtkHybrid.so
poisson_convergence: /usr/lib/libvtkWidgets.so
poisson_convergence: /usr/lib/libvtkParallel.so
poisson_convergence: /usr/lib/libvtkInfovis.so
poisson_convergence: /usr/lib/libvtkGeovis.so
poisson_convergence: /usr/lib/libvtkViews.so
poisson_convergence: /usr/lib/libvtkCharts.so
poisson_convergence: CMakeFiles/poisson_convergence.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable poisson_convergence"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/poisson_convergence.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/poisson_convergence.dir/build: poisson_convergence
.PHONY : CMakeFiles/poisson_convergence.dir/build

CMakeFiles/poisson_convergence.dir/requires: CMakeFiles/poisson_convergence.dir/main.cpp.o.requires
.PHONY : CMakeFiles/poisson_convergence.dir/requires

CMakeFiles/poisson_convergence.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/poisson_convergence.dir/cmake_clean.cmake
.PHONY : CMakeFiles/poisson_convergence.dir/clean

CMakeFiles/poisson_convergence.dir/depend:
	cd /home/fenny/documents/NumericalMethodsOfPDE/GitBucket/Homeworks/hw4/pb2bi/build_convergence && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/fenny/Work/TerraFERMA/share/terraferma/cpp /home/fenny/Work/TerraFERMA/share/terraferma/cpp /home/fenny/documents/NumericalMethodsOfPDE/GitBucket/Homeworks/hw4/pb2bi/build_convergence /home/fenny/documents/NumericalMethodsOfPDE/GitBucket/Homeworks/hw4/pb2bi/build_convergence /home/fenny/documents/NumericalMethodsOfPDE/GitBucket/Homeworks/hw4/pb2bi/build_convergence/CMakeFiles/poisson_convergence.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/poisson_convergence.dir/depend

