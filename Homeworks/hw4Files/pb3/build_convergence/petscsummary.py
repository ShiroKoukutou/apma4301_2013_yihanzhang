
#------ PETSc Performance Summary ----------

Nproc = 1
Time = [   1.347e+02,]
Objects = [   2.070e+02,]
Flops = [   3.152e+10,]
Memory = [   0.000e+00,]
MPIMessages = [   0.000e+00,]
MPIMessageLengths = [   2.000e+01,]
MPIReductions = [   2.016e+03,]

#Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
#                       Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
#  0:      Main Stage: 1.3470e+02 100.0%  3.1523e+10 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  2.530e+02  12.5% 

# Event
# ------------------------------------------------------
class Stage(object):
    def __init__(self, name, time, flops, numMessages, messageLength, numReductions):
        # The time and flops represent totals across processes, whereas reductions are only counted once
        self.name          = name
        self.time          = time
        self.flops         = flops
        self.numMessages   = numMessages
        self.messageLength = messageLength
        self.numReductions = numReductions
        self.event         = {}
class Dummy(object):
    pass
Main_Stage = Stage('Main_Stage', 134.704, 3.15227e+10, 0, 0, 253)
#
VecMax = Dummy()
Main_Stage.event['VecMax'] = VecMax
VecMax.Count = [         4,]
VecMax.Time  = [   3.296e-03,]
VecMax.Flops = [   0.000e+00,]
VecMax.NumMessages = [   0.0e+00,]
VecMax.MessageLength = [   0.000e+00,]
VecMax.NumReductions = [   0.0e+00,]
#
VecMin = Dummy()
Main_Stage.event['VecMin'] = VecMin
VecMin.Count = [         4,]
VecMin.Time  = [   3.247e-03,]
VecMin.Flops = [   0.000e+00,]
VecMin.NumMessages = [   0.0e+00,]
VecMin.MessageLength = [   0.000e+00,]
VecMin.NumReductions = [   0.0e+00,]
#
VecMDot = Dummy()
Main_Stage.event['VecMDot'] = VecMDot
VecMDot.Count = [        30,]
VecMDot.Time  = [   3.958e-02,]
VecMDot.Flops = [   3.340e+07,]
VecMDot.NumMessages = [   0.0e+00,]
VecMDot.MessageLength = [   0.000e+00,]
VecMDot.NumReductions = [   0.0e+00,]
#
VecNorm = Dummy()
Main_Stage.event['VecNorm'] = VecNorm
VecNorm.Count = [       819,]
VecNorm.Time  = [   4.572e-01,]
VecNorm.Flops = [   4.204e+08,]
VecNorm.NumMessages = [   0.0e+00,]
VecNorm.MessageLength = [   0.000e+00,]
VecNorm.NumReductions = [   0.0e+00,]
#
VecScale = Dummy()
Main_Stage.event['VecScale'] = VecScale
VecScale.Count = [      9443,]
VecScale.Time  = [   2.135e+00,]
VecScale.Flops = [   9.562e+08,]
VecScale.NumMessages = [   0.0e+00,]
VecScale.MessageLength = [   0.000e+00,]
VecScale.NumReductions = [   0.0e+00,]
#
VecCopy = Dummy()
Main_Stage.event['VecCopy'] = VecCopy
VecCopy.Count = [      1587,]
VecCopy.Time  = [   1.214e-01,]
VecCopy.Flops = [   0.000e+00,]
VecCopy.NumMessages = [   0.0e+00,]
VecCopy.MessageLength = [   0.000e+00,]
VecCopy.NumReductions = [   0.0e+00,]
#
VecSet = Dummy()
Main_Stage.event['VecSet'] = VecSet
VecSet.Count = [      6431,]
VecSet.Time  = [   2.130e-01,]
VecSet.Flops = [   0.000e+00,]
VecSet.NumMessages = [   0.0e+00,]
VecSet.MessageLength = [   0.000e+00,]
VecSet.NumReductions = [   0.0e+00,]
#
VecAXPY = Dummy()
Main_Stage.event['VecAXPY'] = VecAXPY
VecAXPY.Count = [     18824,]
VecAXPY.Time  = [   8.663e+00,]
VecAXPY.Flops = [   3.813e+09,]
VecAXPY.NumMessages = [   0.0e+00,]
VecAXPY.MessageLength = [   0.000e+00,]
VecAXPY.NumReductions = [   0.0e+00,]
#
VecAYPX = Dummy()
Main_Stage.event['VecAYPX'] = VecAYPX
VecAYPX.Count = [     19604,]
VecAYPX.Time  = [   9.227e+00,]
VecAYPX.Flops = [   2.588e+09,]
VecAYPX.NumMessages = [   0.0e+00,]
VecAYPX.MessageLength = [   0.000e+00,]
VecAYPX.NumReductions = [   0.0e+00,]
#
VecMAXPY = Dummy()
Main_Stage.event['VecMAXPY'] = VecMAXPY
VecMAXPY.Count = [        33,]
VecMAXPY.Time  = [   4.919e-02,]
VecMAXPY.Flops = [   3.948e+07,]
VecMAXPY.NumMessages = [   0.0e+00,]
VecMAXPY.MessageLength = [   0.000e+00,]
VecMAXPY.NumReductions = [   0.0e+00,]
#
VecAssemblyBegin = Dummy()
Main_Stage.event['VecAssemblyBegin'] = VecAssemblyBegin
VecAssemblyBegin.Count = [        26,]
VecAssemblyBegin.Time  = [   4.111e-05,]
VecAssemblyBegin.Flops = [   0.000e+00,]
VecAssemblyBegin.NumMessages = [   0.0e+00,]
VecAssemblyBegin.MessageLength = [   0.000e+00,]
VecAssemblyBegin.NumReductions = [   0.0e+00,]
#
VecAssemblyEnd = Dummy()
Main_Stage.event['VecAssemblyEnd'] = VecAssemblyEnd
VecAssemblyEnd.Count = [        26,]
VecAssemblyEnd.Time  = [   2.957e-05,]
VecAssemblyEnd.Flops = [   0.000e+00,]
VecAssemblyEnd.NumMessages = [   0.0e+00,]
VecAssemblyEnd.MessageLength = [   0.000e+00,]
VecAssemblyEnd.NumReductions = [   0.0e+00,]
#
VecPointwiseMult = Dummy()
Main_Stage.event['VecPointwiseMult'] = VecPointwiseMult
VecPointwiseMult.Count = [     14148,]
VecPointwiseMult.Time  = [   9.762e+00,]
VecPointwiseMult.Flops = [   1.433e+09,]
VecPointwiseMult.NumMessages = [   0.0e+00,]
VecPointwiseMult.MessageLength = [   0.000e+00,]
VecPointwiseMult.NumReductions = [   0.0e+00,]
#
VecSetRandom = Dummy()
Main_Stage.event['VecSetRandom'] = VecSetRandom
VecSetRandom.Count = [         3,]
VecSetRandom.Time  = [   1.235e-02,]
VecSetRandom.Flops = [   0.000e+00,]
VecSetRandom.NumMessages = [   0.0e+00,]
VecSetRandom.MessageLength = [   0.000e+00,]
VecSetRandom.NumReductions = [   0.0e+00,]
#
VecNormalize = Dummy()
Main_Stage.event['VecNormalize'] = VecNormalize
VecNormalize.Count = [        33,]
VecNormalize.Time  = [   1.105e-02,]
VecNormalize.Flops = [   1.002e+07,]
VecNormalize.NumMessages = [   0.0e+00,]
VecNormalize.MessageLength = [   0.000e+00,]
VecNormalize.NumReductions = [   0.0e+00,]
#
MatMult = Dummy()
Main_Stage.event['MatMult'] = MatMult
MatMult.Count = [     14929,]
MatMult.Time  = [   6.545e+01,]
MatMult.Flops = [   2.124e+10,]
MatMult.NumMessages = [   0.0e+00,]
MatMult.MessageLength = [   0.000e+00,]
MatMult.NumReductions = [   0.0e+00,]
#
MatMultAdd = Dummy()
Main_Stage.event['MatMultAdd'] = MatMultAdd
MatMultAdd.Count = [      2352,]
MatMultAdd.Time  = [   2.827e+00,]
MatMultAdd.Flops = [   4.738e+08,]
MatMultAdd.NumMessages = [   0.0e+00,]
MatMultAdd.MessageLength = [   0.000e+00,]
MatMultAdd.NumReductions = [   0.0e+00,]
#
MatMultTranspose = Dummy()
Main_Stage.event['MatMultTranspose'] = MatMultTranspose
MatMultTranspose.Count = [      2352,]
MatMultTranspose.Time  = [   2.221e+00,]
MatMultTranspose.Flops = [   4.738e+08,]
MatMultTranspose.NumMessages = [   0.0e+00,]
MatMultTranspose.MessageLength = [   0.000e+00,]
MatMultTranspose.NumReductions = [   0.0e+00,]
#
MatSolve = Dummy()
Main_Stage.event['MatSolve'] = MatSolve
MatSolve.Count = [       784,]
MatSolve.Time  = [   3.446e-02,]
MatSolve.Flops = [   1.530e+07,]
MatSolve.NumMessages = [   0.0e+00,]
MatSolve.MessageLength = [   0.000e+00,]
MatSolve.NumReductions = [   0.0e+00,]
#
MatLUFactorSym = Dummy()
Main_Stage.event['MatLUFactorSym'] = MatLUFactorSym
MatLUFactorSym.Count = [         1,]
MatLUFactorSym.Time  = [   3.411e-04,]
MatLUFactorSym.Flops = [   0.000e+00,]
MatLUFactorSym.NumMessages = [   0.0e+00,]
MatLUFactorSym.MessageLength = [   0.000e+00,]
MatLUFactorSym.NumReductions = [   3.0e+00,]
#
MatLUFactorNum = Dummy()
Main_Stage.event['MatLUFactorNum'] = MatLUFactorNum
MatLUFactorNum.Count = [         1,]
MatLUFactorNum.Time  = [   5.741e-04,]
MatLUFactorNum.Flops = [   1.782e+05,]
MatLUFactorNum.NumMessages = [   0.0e+00,]
MatLUFactorNum.MessageLength = [   0.000e+00,]
MatLUFactorNum.NumReductions = [   0.0e+00,]
#
MatConvert = Dummy()
Main_Stage.event['MatConvert'] = MatConvert
MatConvert.Count = [         3,]
MatConvert.Time  = [   3.540e-02,]
MatConvert.Flops = [   0.000e+00,]
MatConvert.NumMessages = [   0.0e+00,]
MatConvert.MessageLength = [   0.000e+00,]
MatConvert.NumReductions = [   0.0e+00,]
#
MatScale = Dummy()
Main_Stage.event['MatScale'] = MatScale
MatScale.Count = [         3,]
MatScale.Time  = [   1.807e-02,]
MatScale.Flops = [   4.239e+06,]
MatScale.NumMessages = [   0.0e+00,]
MatScale.MessageLength = [   0.000e+00,]
MatScale.NumReductions = [   0.0e+00,]
#
MatAssemblyBegin = Dummy()
Main_Stage.event['MatAssemblyBegin'] = MatAssemblyBegin
MatAssemblyBegin.Count = [        45,]
MatAssemblyBegin.Time  = [   8.036e-05,]
MatAssemblyBegin.Flops = [   0.000e+00,]
MatAssemblyBegin.NumMessages = [   0.0e+00,]
MatAssemblyBegin.MessageLength = [   0.000e+00,]
MatAssemblyBegin.NumReductions = [   0.0e+00,]
#
MatAssemblyEnd = Dummy()
Main_Stage.event['MatAssemblyEnd'] = MatAssemblyEnd
MatAssemblyEnd.Count = [        45,]
MatAssemblyEnd.Time  = [   1.622e-01,]
MatAssemblyEnd.Flops = [   0.000e+00,]
MatAssemblyEnd.NumMessages = [   0.0e+00,]
MatAssemblyEnd.MessageLength = [   0.000e+00,]
MatAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatGetRow = Dummy()
Main_Stage.event['MatGetRow'] = MatGetRow
MatGetRow.Count = [    911034,]
MatGetRow.Time  = [   1.145e+00,]
MatGetRow.Flops = [   0.000e+00,]
MatGetRow.NumMessages = [   0.0e+00,]
MatGetRow.MessageLength = [   0.000e+00,]
MatGetRow.NumReductions = [   0.0e+00,]
#
MatGetRowIJ = Dummy()
Main_Stage.event['MatGetRowIJ'] = MatGetRowIJ
MatGetRowIJ.Count = [         1,]
MatGetRowIJ.Time  = [   5.194e-05,]
MatGetRowIJ.Flops = [   0.000e+00,]
MatGetRowIJ.NumMessages = [   0.0e+00,]
MatGetRowIJ.MessageLength = [   0.000e+00,]
MatGetRowIJ.NumReductions = [   0.0e+00,]
#
MatGetOrdering = Dummy()
Main_Stage.event['MatGetOrdering'] = MatGetOrdering
MatGetOrdering.Count = [         1,]
MatGetOrdering.Time  = [   4.011e-04,]
MatGetOrdering.Flops = [   0.000e+00,]
MatGetOrdering.NumMessages = [   0.0e+00,]
MatGetOrdering.MessageLength = [   0.000e+00,]
MatGetOrdering.NumReductions = [   2.0e+00,]
#
MatCoarsen = Dummy()
Main_Stage.event['MatCoarsen'] = MatCoarsen
MatCoarsen.Count = [         3,]
MatCoarsen.Time  = [   7.577e-02,]
MatCoarsen.Flops = [   0.000e+00,]
MatCoarsen.NumMessages = [   0.0e+00,]
MatCoarsen.MessageLength = [   0.000e+00,]
MatCoarsen.NumReductions = [   9.0e+00,]
#
MatZeroEntries = Dummy()
Main_Stage.event['MatZeroEntries'] = MatZeroEntries
MatZeroEntries.Count = [         3,]
MatZeroEntries.Time  = [   9.362e-03,]
MatZeroEntries.Flops = [   0.000e+00,]
MatZeroEntries.NumMessages = [   0.0e+00,]
MatZeroEntries.MessageLength = [   0.000e+00,]
MatZeroEntries.NumReductions = [   0.0e+00,]
#
MatView = Dummy()
Main_Stage.event['MatView'] = MatView
MatView.Count = [         6,]
MatView.Time  = [   6.948e-04,]
MatView.Flops = [   0.000e+00,]
MatView.NumMessages = [   0.0e+00,]
MatView.MessageLength = [   0.000e+00,]
MatView.NumReductions = [   0.0e+00,]
#
MatTranspose = Dummy()
Main_Stage.event['MatTranspose'] = MatTranspose
MatTranspose.Count = [         3,]
MatTranspose.Time  = [   1.439e-01,]
MatTranspose.Flops = [   0.000e+00,]
MatTranspose.NumMessages = [   0.0e+00,]
MatTranspose.MessageLength = [   0.000e+00,]
MatTranspose.NumReductions = [   6.0e+00,]
#
MatPtAP = Dummy()
Main_Stage.event['MatPtAP'] = MatPtAP
MatPtAP.Count = [         6,]
MatPtAP.Time  = [   3.178e-01,]
MatPtAP.Flops = [   1.703e+07,]
MatPtAP.NumMessages = [   0.0e+00,]
MatPtAP.MessageLength = [   0.000e+00,]
MatPtAP.NumReductions = [   3.6e+01,]
#
MatPtAPSymbolic = Dummy()
Main_Stage.event['MatPtAPSymbolic'] = MatPtAPSymbolic
MatPtAPSymbolic.Count = [         6,]
MatPtAPSymbolic.Time  = [   2.221e-01,]
MatPtAPSymbolic.Flops = [   0.000e+00,]
MatPtAPSymbolic.NumMessages = [   0.0e+00,]
MatPtAPSymbolic.MessageLength = [   0.000e+00,]
MatPtAPSymbolic.NumReductions = [   3.6e+01,]
#
MatPtAPNumeric = Dummy()
Main_Stage.event['MatPtAPNumeric'] = MatPtAPNumeric
MatPtAPNumeric.Count = [         6,]
MatPtAPNumeric.Time  = [   9.569e-02,]
MatPtAPNumeric.Flops = [   1.703e+07,]
MatPtAPNumeric.NumMessages = [   0.0e+00,]
MatPtAPNumeric.MessageLength = [   0.000e+00,]
MatPtAPNumeric.NumReductions = [   0.0e+00,]
#
MatTrnMatMult = Dummy()
Main_Stage.event['MatTrnMatMult'] = MatTrnMatMult
MatTrnMatMult.Count = [         3,]
MatTrnMatMult.Time  = [   4.161e-01,]
MatTrnMatMult.Flops = [   1.652e+07,]
MatTrnMatMult.NumMessages = [   0.0e+00,]
MatTrnMatMult.MessageLength = [   0.000e+00,]
MatTrnMatMult.NumReductions = [   1.2e+01,]
#
MatGetSymTrans = Dummy()
Main_Stage.event['MatGetSymTrans'] = MatGetSymTrans
MatGetSymTrans.Count = [         3,]
MatGetSymTrans.Time  = [   1.855e-02,]
MatGetSymTrans.Flops = [   0.000e+00,]
MatGetSymTrans.NumMessages = [   0.0e+00,]
MatGetSymTrans.MessageLength = [   0.000e+00,]
MatGetSymTrans.NumReductions = [   0.0e+00,]
#
SNESSolve = Dummy()
Main_Stage.event['SNESSolve'] = SNESSolve
SNESSolve.Count = [         1,]
SNESSolve.Time  = [   1.096e+02,]
SNESSolve.Flops = [   3.137e+10,]
SNESSolve.NumMessages = [   0.0e+00,]
SNESSolve.MessageLength = [   0.000e+00,]
SNESSolve.NumReductions = [   2.5e+01,]
#
SNESFunctionEval = Dummy()
Main_Stage.event['SNESFunctionEval'] = SNESFunctionEval
SNESFunctionEval.Count = [         1,]
SNESFunctionEval.Time  = [   3.793e+00,]
SNESFunctionEval.Flops = [   0.000e+00,]
SNESFunctionEval.NumMessages = [   0.0e+00,]
SNESFunctionEval.MessageLength = [   0.000e+00,]
SNESFunctionEval.NumReductions = [   0.0e+00,]
#
SNESJacobianEval = Dummy()
Main_Stage.event['SNESJacobianEval'] = SNESJacobianEval
SNESJacobianEval.Count = [         1,]
SNESJacobianEval.Time  = [   4.323e+00,]
SNESJacobianEval.Flops = [   0.000e+00,]
SNESJacobianEval.NumMessages = [   0.0e+00,]
SNESJacobianEval.MessageLength = [   0.000e+00,]
SNESJacobianEval.NumReductions = [   0.0e+00,]
#
KSPGMRESOrthog = Dummy()
Main_Stage.event['KSPGMRESOrthog'] = KSPGMRESOrthog
KSPGMRESOrthog.Count = [        30,]
KSPGMRESOrthog.Time  = [   8.147e-02,]
KSPGMRESOrthog.Flops = [   6.681e+07,]
KSPGMRESOrthog.NumMessages = [   0.0e+00,]
KSPGMRESOrthog.MessageLength = [   0.000e+00,]
KSPGMRESOrthog.NumReductions = [   0.0e+00,]
#
KSPSetUp = Dummy()
Main_Stage.event['KSPSetUp'] = KSPSetUp
KSPSetUp.Count = [        14,]
KSPSetUp.Time  = [   2.386e-02,]
KSPSetUp.Flops = [   0.000e+00,]
KSPSetUp.NumMessages = [   0.0e+00,]
KSPSetUp.MessageLength = [   0.000e+00,]
KSPSetUp.NumReductions = [   3.9e+01,]
#
KSPSolve = Dummy()
Main_Stage.event['KSPSolve'] = KSPSolve
KSPSolve.Count = [         1,]
KSPSolve.Time  = [   1.015e+02,]
KSPSolve.Flops = [   3.137e+10,]
KSPSolve.NumMessages = [   0.0e+00,]
KSPSolve.MessageLength = [   0.000e+00,]
KSPSolve.NumReductions = [   2.5e+01,]
#
PCSetUp = Dummy()
Main_Stage.event['PCSetUp'] = PCSetUp
PCSetUp.Count = [         3,]
PCSetUp.Time  = [   4.171e+00,]
PCSetUp.Flops = [   1.642e+08,]
PCSetUp.NumMessages = [   0.0e+00,]
PCSetUp.MessageLength = [   0.000e+00,]
PCSetUp.NumReductions = [   2.0e+02,]
#
PCSetUpOnBlocks = Dummy()
Main_Stage.event['PCSetUpOnBlocks'] = PCSetUpOnBlocks
PCSetUpOnBlocks.Count = [       784,]
PCSetUpOnBlocks.Time  = [   3.017e-03,]
PCSetUpOnBlocks.Flops = [   1.782e+05,]
PCSetUpOnBlocks.NumMessages = [   0.0e+00,]
PCSetUpOnBlocks.MessageLength = [   0.000e+00,]
PCSetUpOnBlocks.NumReductions = [   5.0e+00,]
#
PCApply = Dummy()
Main_Stage.event['PCApply'] = PCApply
PCApply.Count = [     14899,]
PCApply.Time  = [   9.850e+00,]
PCApply.Flops = [   1.445e+09,]
PCApply.NumMessages = [   0.0e+00,]
PCApply.MessageLength = [   0.000e+00,]
PCApply.NumReductions = [   0.0e+00,]
#
PCGAMGgraph_AGG = Dummy()
Main_Stage.event['PCGAMGgraph_AGG'] = PCGAMGgraph_AGG
PCGAMGgraph_AGG.Count = [         3,]
PCGAMGgraph_AGG.Time  = [   2.796e+00,]
PCGAMGgraph_AGG.Flops = [   4.239e+06,]
PCGAMGgraph_AGG.NumMessages = [   0.0e+00,]
PCGAMGgraph_AGG.MessageLength = [   0.000e+00,]
PCGAMGgraph_AGG.NumReductions = [   1.8e+01,]
#
PCGAMGcoarse_AGG = Dummy()
Main_Stage.event['PCGAMGcoarse_AGG'] = PCGAMGcoarse_AGG
PCGAMGcoarse_AGG.Count = [         3,]
PCGAMGcoarse_AGG.Time  = [   5.517e-01,]
PCGAMGcoarse_AGG.Flops = [   1.652e+07,]
PCGAMGcoarse_AGG.NumMessages = [   0.0e+00,]
PCGAMGcoarse_AGG.MessageLength = [   0.000e+00,]
PCGAMGcoarse_AGG.NumReductions = [   2.4e+01,]
#
PCGAMGProl_AGG = Dummy()
Main_Stage.event['PCGAMGProl_AGG'] = PCGAMGProl_AGG
PCGAMGProl_AGG.Count = [         3,]
PCGAMGProl_AGG.Time  = [   1.512e-01,]
PCGAMGProl_AGG.Flops = [   0.000e+00,]
PCGAMGProl_AGG.NumMessages = [   0.0e+00,]
PCGAMGProl_AGG.MessageLength = [   0.000e+00,]
PCGAMGProl_AGG.NumReductions = [   1.2e+01,]
#
PCGAMGPOpt_AGG = Dummy()
Main_Stage.event['PCGAMGPOpt_AGG'] = PCGAMGPOpt_AGG
PCGAMGPOpt_AGG.Count = [         3,]
PCGAMGPOpt_AGG.Time  = [   1.091e-05,]
PCGAMGPOpt_AGG.Flops = [   0.000e+00,]
PCGAMGPOpt_AGG.NumMessages = [   0.0e+00,]
PCGAMGPOpt_AGG.MessageLength = [   0.000e+00,]
PCGAMGPOpt_AGG.NumReductions = [   0.0e+00,]
# ========================================================================================================================
AveragetimetogetPetscTime = 1.11101e-06

