# -*- coding: utf-8 -*-
"""
Stability diagrams:  draws stability diagrams for various ODE schemes
Created on Tue Nov 13 10:13:08 2012

@author: mspieg
"""
import numpy as np
import pylab as pl
from scipy.optimize import brentq

def plotR(x,y,R,title=None):
    pl.contourf(x,y,np.abs(R),[0.,1.])
    pl.colorbar()
    pl.axis('tight')
    pl.grid()
    if title:
        pl.title(title)
    return
    
bnd=13.    
x=np.linspace(-bnd,bnd,100)
y=np.linspace(-bnd,bnd,100)
X,Y = np.meshgrid(x,y)
Z = X + Y*1j
V=np.array([1.])

def rfunction(Z):
    return abs((12.+5.*Z)/((3.-Z)*(4.-Z))) - 1
  
#print brentq(rfunction,(-1)*bnd, -2)

# Euler
R = (12.+5.*Z)/((3.-Z)*(4.-Z))
pl.figure()
plotR(x,y,R,'TR-BDF2')

pl.savefig('../figures/pb1f.eps')