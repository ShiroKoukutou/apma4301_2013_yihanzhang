# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 00:36:34 2013

@author: fenny
"""

import numpy as np
import pylab as pl
from stupidode import stupidode

def func(x):
    return np.array(\
	[np.pi*np.sin(0.5*np.pi*x[0])*np.cos(np.pi*x[1]),\
	-0.5*np.pi*np.cos(0.5*np.pi*x[0])*np.sin(np.pi*x[1])])
 
def val(x):
    return np.sin(np.pi/2*x[0])*np.sin(np.pi*x[1])
    
ode = stupidode(func)

#N = np.array([10**i for i in np.linspace(2.5,3.5,3)])
#V = np.zeros(3)

N = np.array([3])
V = np.zeros(1)

for i in range(len(N)):
    print N[i]
    ode.set_integrator('bdf1',nsteps=int(np.floor(N[i])))
    ode.set_initial_value(np.array([1.5,0.5]),0.)
    ode.integrate(15.)
    V[i] = abs(val(ode.y)\
    - val(np.array([1.5,0.5])))/val(np.array([1.5,0.5]))

p = np.polyfit(np.log(N),np.log(V),1)
pl.figure()
pl.loglog(N,V)
pl.xlabel('Number of time steps')
pl.ylabel('Relative error')
pl.title('Converge rate Error vs. N p = {0}'.format(p[0]))