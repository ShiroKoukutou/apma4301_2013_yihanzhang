# -*- coding: utf-8 -*-
"""
Demonstration code for solving stiff systems of equations in scipy

Created on Wed Nov 14 22:27:41 2012

@author: mspieg

 an example with stiff linear systems: radioactive decay chains

 Radioactive Decay chains
 The decay of a system of radioactive nuclides u_1->u_2->u_3-> ... (e.g U -> Th -> Ra) can be
 described by the linear dynamical system

 $$ \frac{d u_i}{d t} = \lambda_{i-1}u_{i-1} - \lambda_i u_i $$
 
 where $\lambda_i$ are the decay constants related to the half-lives by
 $\ln (2)/t_{1/2}$ and $\lambda_0 =0$.

 this system can be written compactly as $du/dt = Lu$ where $L$ is a
 lower triangular matrix matrix with $-\lambda_i$ on the diagonal.  For
 scaling and numerical accuracy, however, it is actually better to solve
# the scaled system for the *activities* $a_i = \lambda_i u_i$, or in
 vector notation $a = Du$, where $D$ is a diagonal matrix with $\lambda_i$
 on the diagonal.  Multiplying both sides of $du/dt =Lu$ by $D$ gives the
 transformed equations

 $$ \frac{d a}{d t} = A a $$

 where $A = DLD^{-1}$.

 Here we will explore the behavior of this system numerically for
 different sets of half-lives

"""
from scipy.integrate import ode
from scipy.linalg import expm
from scipy import dot
from numpy import *
import pylab as pl

#set line parameters
pl.rc('lines',linewidth=2)
pl.rc('font',weight='bold')

## the RHS function to be passed to ODExx

def decayChain(t,u,lambdas):
    """
    DECAYCHAIN  describes RHS for radioactive decay of a chain of  n nuclides such that
          element_1 -> element_2 -> element_3 ... with decay constants lambda_1, lambda_2 etc
          
          the problem can be written as a linear dynamical system  du/dt = Au with
          
          A = [ -lambda_1               0                0 ;
                 lambda_2 -lambda_2                0 ;
                              0  lambda_3 - lambda_3 ]
                              etc.
                              
                              
                              t- current time
                              u -  vector of activities of decay chain nuclides
                              lambda - vector of decay  constants
                              
"""
    # matrix free representation of sparse A*u    
    f = zeros(u.shape)
    f[0] = -lambdas[0]*u[0]
    for k in range(1,len(u)):
        f[k] = lambdas[k]*(u[k-1]-u[k])
    
    return f
    
def jacobian(t,u,lambdas):
    """returns matrix of decay constants
    """
    A = zeros((len(lambdas),len(lambdas)))
    A[0,0] = -lambdas[0]
    for k in range(1,len(lambdas)):
        A[k,k-1:k+1]=lambdas[k]*array([1.,-1])
        
    return A
        
        

def runode(ode,u0,t0,tmax,nsteps,showoutput=False):
    """  wrapper function to call ODE for fixed number of steps and return arrays
    """
    t = linspace(t0,tmax,nsteps)
    u = zeros((nsteps,len(u0)))
    ode.set_initial_value(u0,t0)
    # hugely wasteful but evaluate the odeintegrator at points t
    for k in xrange(len(t)):
        ode.set_initial_value(ode.y,ode.t)
        ode.integrate(t[k])
        u[k,:] = ode.y
        if showoutput:
            print t[k], u[k,:]

    return t,u
    
def plotode(t,u,thalf,title=None):
    """ utility function for plotting the output of the ODE's
    """
    pl.plot(t,u)
    pl.xlabel('time (yrs)')
    pl.ylabel('Activities')
    if title:
        pl.title(title)
    pl.legend(thalf,title="half-lives",loc="best")
    
    
    

# Half lives for U238 -> Th230 -> Ra226 (in yrs) (non-stiff)
tHalf = array([ 4.e9, 75200., 1600. ])

#decay constants
lambdas = log(2.)/tHalf;

t0 = 0.
a0 = array([ 1., 2., 0.])
tmax = 100000.

# calculate exact solution
A = jacobian(t0,a0,lambdas)
a_exact = dot(expm(A*tmax),a0)


# set options
RelTol = 1.e-6
AbsTol = 1.e-8

print '\n############# non-stiff solution ###############'
## initialize  the ODE solver using a Dormand Prince 56 pair
ode45 = ode(decayChain)
ode45.set_integrator('dopri5', atol=AbsTol, rtol=RelTol,nsteps=10000)
ode45.set_f_params(lambdas)
t,a = runode(ode45,a0,t0,tmax,100)
print 'exact   Non-stiff solution t=',tmax,' activities = ',a_exact
print 'dopri5  Non-stiff solution t=',t[-1],' activities = ',a[-1,:], \
  'relative error =',abs(a_exact - a[-1,:])/a_exact
print '################################################\n'

pl.figure()
plotode(t,a,tHalf,'DormandPrince 56')

# make the problem stiff
# add one more very short lived nuclide (Th238) with half-life 24.1 days

# Half lives for U238 -> Th238->Th230 -> Ra226 (in yrs) (stiff)
tHalf = array([ 4.e9, 24.1/365.25, 75200., 1600. ])
lambdas = log(2.)/tHalf
a0 = [ 1., 2., 2., 0.]

# calculate exact solution
A = jacobian(t0,a0,lambdas)
a_exact = dot(expm(A*tmax),a0)

ode45.set_f_params(lambdas)
t,a = runode(ode45,a0,t0,tmax,100,showoutput=False)
print '\n############# stiff solution ##################'
print 'exact     stiff solution t=',tmax,' activities = ',a_exact
print 'dopri5    stiff solution t=',t[-1],' activities = ',a[-1,:], \
  'relative error =',abs(a_exact - a[-1,:])/a_exact
pl.figure()
plotode(t,a,tHalf,'DormandPrince 56- Stiff')

# now use a proper stiff integrator
odebdf= ode(decayChain,jacobian)
odebdf.set_integrator('vode',method='bdf',with_jacobian=True,atol=AbsTol, rtol=RelTol,nsteps=10000)
odebdf.set_f_params(lambdas)
odebdf.set_jac_params(lambdas)
odebdf.set_initial_value(a0,t0)


t,a = runode(odebdf,a0,t0,tmax,101)
print 'vode(bdf) stiff solution t=',t[-1],' activities = ',a[-1,:], \
   'relative error =',abs(a_exact - a[-1,:])/a_exact
print '################################################\n'


pl.figure()
plotode(t,a,tHalf,'bdf scheme')




pl.show(block=True)
