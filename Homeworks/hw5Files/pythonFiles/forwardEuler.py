# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 11:34:45 2013

@author: fenny
"""

import numpy as np
from stupidode import stupidode

def func(t,x):
    return np.array(\
	[np.pi*np.sin(0.5*np.pi*x[0])*np.cos(np.pi*x[1]),\
	-0.5*np.pi*np.cos(0.5*np.pi*x[0])*np.sin(np.pi*x[1])])
    
ode = stupidode(func)

N = np.linspace(1,6,11)
ode.set_integrator('euler',nsteps=1000)
ode.set_initial_value(np.array([1.5,0.5]),0.)
ode.integrate(15.)
print ode.t, ode.y