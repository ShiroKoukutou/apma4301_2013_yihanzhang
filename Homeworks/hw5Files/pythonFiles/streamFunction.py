# -*- coding: utf-8 -*-
"""
Demonstration code for solving stiff systems of equations in scipy

Created on Wed Nov 14 22:27:41 2012

@author: mspieg

"""
from scipy.integrate import ode
from numpy import *
import pylab as pl

## the RHS function to be passed to ODExx

def streamFunction(t,u,xparam):
    """
    STREAM FUNCTION: We are solving the ODE problem with stream function
                    \psi = sin(\pi/2 x)sin(\pi y).
                    Take the curve of that, we could get the RHS:
                    V = (\partial\psi/\partial y,-\partial\psi/\partial x)
                    i.e.
                    V = (\pi sin(\pi/2 x)cos(\pi y),
                         -\pi/2 cos(\pi/2 x)sin(\pi y))
                              
"""
    # matrix free representation of sparse A*u    
    f = zeros((2,1))
    f[0] = xparam[1]*sin(xparam[0]*u[0])*cos(xparam[1]*u[1])
    f[1] = -xparam[0]*cos(xparam[0]*u[0])*sin(xparam[1]*u[1])
    
    return f        

def runode(ode,u0,t0,tmax,nsteps,showoutput=False):
    """  wrapper function to call ODE for fixed number of steps and return arrays
    """
    t = linspace(t0,tmax,nsteps)
    u = zeros((nsteps,len(u0)))
    ode.set_initial_value(u0,t0)
    # hugely wasteful but evaluate the odeintegrator at points t
    for k in xrange(len(t)):
        ode.set_initial_value(ode.y,ode.t)
        ode.integrate(t[k])
        u[k,:] = ode.y
        if showoutput:
            print t[k], u[k,:]

    return t,u
    
def plotStream(u,title=None):
    """ utility function for plotting the output of the ODE's
    """
    pl.plot(u[:,0],u[:,1])
    pl.xlabel('x')
    pl.ylabel('y')
    if title:
        pl.title(title)
        
def psi(a,xparam):
    """ Return the value of psi at a certain point a
    """
    return sin(xparam[0]*a[0])*sin(xparam[1]*a[1])

def calcStreamError(a,a0,xparam):
    """ To calculate the relative error according to the function \psi
    """
    return (psi(a,xparam)-psi(a0,xparam))/psi(a0,xparam)
    

# The coefficient of \vec{x}
xparam = [ pi/2., pi ]

t0 = 0.
a0 = array([ 1.5, 0.5])
tmax = 15.

a_exact = a0.copy()

# set options
RelTol = 1.e-6
AbsTol = 1.e-8

print '\n############# non-stiff solution ###############'
## initialize  the ODE solver using a Dormand Prince 56 pair
ode45 = ode(streamFunction)
ode45.set_integrator('dopri5',method='bdf', atol=AbsTol, rtol=RelTol,nsteps=10000)
ode45.set_f_params(xparam)

t,a = runode(ode45,a0,t0,tmax,100)
print 'exact   Non-stiff solution t=',tmax,' activities = ',a_exact
print 'dopri5  Non-stiff solution t=',t[-1],' activities = ',a[-1,:], \
  'relative error =',calcStreamError(a[-1,:],a_exact,xparam)
print '################################################\n'

pl.figure()
plotStream(a,'DormandPrince 56 - BDF Scheme')

# Use vode-bdf scheme to solve
odebdf= ode(streamFunction)
odebdf.set_integrator('vode',method='bdf',with_jacobian=True,atol=AbsTol, rtol=RelTol,nsteps=10000)
odebdf.set_f_params(xparam)
odebdf.set_initial_value(a0,t0)


t,a = runode(odebdf,a0,t0,tmax,101)
print 'vode(bdf) stiff solution t=',t[-1],' activities = ',a[-1,:], \
   'relative error =',calcStreamError(a[-1,:],a_exact,xparam)
print '################################################\n'

pl.figure()
plotStream(a,'Vode - BDF Scheme')