# -*- coding: utf-8 -*-
"""
Stability diagrams:  draws stability diagrams for various ODE schemes
Created on Tue Nov 13 10:13:08 2012

@author: mspieg
"""
import numpy as np
import pylab as pl
from scipy.optimize import brentq

def plotR(x,y,R,title=None):
    pl.contourf(x,y,np.abs(R),[0.,1.])
    pl.colorbar()
    pl.axis('tight')
    pl.grid()
    if title:
        pl.title(title)
    return
    
bnd=13.    
x=np.linspace(-bnd,bnd,100)
y=np.linspace(-bnd,bnd,100)
X,Y = np.meshgrid(x,y)
Z = X + Y*1j
V=np.array([1.])

def rfunction(z):
    return 1.+z +1
    
print brentq(rfunction,(-1)*bnd,0)

# Euler
R = 1.+Z
pl.figure()
plotR(x,y,R,'Euler')

#pl.savefig('../../Homeworks/hw5/figures/Euler.eps')