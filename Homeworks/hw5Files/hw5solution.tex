% English Version of fe-i-tex

\documentclass{article}

% Set margins:
\hoffset = -0.9in
\voffset = -0.75in
\textheight = 680pt
\textwidth = 470pt

% Mathematic libraries
\usepackage{amsmath}
\usepackage{amssymb}
\newcommand{\diff}{\mathrm{d}}
\newcommand{\me}{\mathrm{e}}
\newcommand{\mi}{\mathrm{i}}
\newcommand{\erf}{\mathrm{erf}}

% Symbol libraries
\usepackage{textcomp}

% Code display library
\usepackage{mylistings}

% Graphic libraries
\usepackage{float}
\usepackage{graphicx}

% Enumerate label libraries
\usepackage{enumerate}

% Font package
\usepackage{times}

\usepackage{setspace}

% TODO: Add your article here.

\begin{document}

\title{Numerical Method for Partial Differential Equations:\\ Solution to Homework \#5}
\author{Yihan Zhang (Uni: yz2567)}
\maketitle

\begin{enumerate}[\bfseries \large Problem 1.]
  \item Basic Stepping Rules, Stability and Accuracy
		{\bf Solution}: (I solve this problem scheme by scheme)
    \begin{enumerate}[\hspace{-0.5in} \bf {Part} (a)]
      \item For forward Euler, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c}
						0 & 0 \\ \hline
						$b_j$ & 1
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				$$
				u_1 = u_0 + k\lambda u_0 = R(z)u_0 \text{,}
				$$
				where,
				$$
				R(z) = 1+z
				$$
				Taylor's series for exponential function goes like:
				\begin{align*}
					\me^z = & 1 + z + \frac{1}{2}z^2 + \frac{1}{6} z^3 + \cdots\\
					= & \sum_{n=0}^\infty \frac{1}{n!}z^n
				\end{align*}
				Thus for forward Euler, compare $R(z)$ with the polynomial series, we could easily find that the step error is $O(k^2)$
				
				The A-stability region is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale = 0.5]{./figures/pb1a.eps}
					\caption{The A-stability region of forward Euler scheme}
				\end{figure}

				If absolute stability is guaranteed, the value of $z$ should be no smaller than $-2$ as shown in the stability plot. Thus the minimum time step $k$ should satisfy
				\begin{align*}
					k \leqslant & -\frac{2}{\lambda}
				\end{align*}
				
      \item For backward Euler, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c}
						1 & 1 \\ \hline
						$b_j$ & 1
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				\begin{align*}
					u_1 = & u_0 + k\lambda u_1 \\
					\therefore u_1 = & \frac{1}{1-z}u_0\\
					= & R(z)u_0 \text{,}
				\end{align*}
				where,
				$$
				R(z) = \frac{1}{1-z}
				$$
				And the polynomial expansion of $R(z)$ is:
				\begin{align*}
					\frac{1}{1-z} = & 1 + z + z^2 + z^3 \cdots\\
					= & \sum_{n=0}^\infty z^n
				\end{align*}
				Compare $R(z)$ with the polynomial series, we could easily find that the step error is $O(k^2)$
				
				The A-stability region is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale = 0.5]{./figures/pb1b.eps}
					\caption{The A-stability region of backward Euler scheme}
				\end{figure}
				The absolute stability is always guaranteed for backward Euler scheme as long as $\lambda$ is real and negative.
				
      \item For mid-point scheme, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c  c}
						0 & 0\\
						1/2 & 1/2 & 0\\ \hline
						$b_j$ & 0 & 1
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				\begin{align*}
					u_1 = & u_0 + k\lambda (u_0 + \frac{k}{2}\lambda u_0) \\
					\therefore u_1 = & (1+z+\frac{z^2}{2}) u_0\\
					= & R(z)u_0 \text{,}
				\end{align*}
				where,
				$$
				R(z) = 1 + z + \frac{z^2}{2}
				$$
				Compare $R(z)$ with the polynomial series, we could easily find that the step error is $O(k^3)$
				
				The A-stability region is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale = 0.5]{./figures/pb1c.eps}
					\caption{The A-stability region of mid-point scheme}
				\end{figure}
				If absolute stability is guaranteed, the value of $z$ should be no smaller than $-2$ as shown in the stability plot. Thus the minimum time step $k$ should satisfy
				\begin{align*}
					k \leqslant & -\frac{2}{\lambda}
				\end{align*}
				
      \item For improved-Euler (Runge-Kutta 2) scheme, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c  c}
						0 & 0\\
						1 & 1 & 0\\ \hline
						$b_j$ & 1/2 & 1/2
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				\begin{align*}
					u_1 = & u_0 + \frac{1}{2}k\lambda (2u_0 + k\lambda u_0) \\
					\therefore u_1 = & (1+z+\frac{z^2}{2}) u_0\\
					= & R(z)u_0 \text{,}
				\end{align*}
				where,
				$$
				R(z) = 1 + z + \frac{z^2}{2}
				$$
				This $R(z)$ is exactly the same with that of mid-point scheme. Therefore, its step error, A-stability region and the minimum time step $k$ is also the same as that of mid-point scheme.
				
      \item For trapezoidal, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c c}
						0 & 0 \\
						1 & 1/2 & 1/2\\ \hline
						$b_j$ & 1/2 1/2
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				\begin{align*}
					u_1 = & u_0 + \frac{1}{2}k\lambda (u_1+u_z) \\
					\therefore u_1 = & \frac{2+z}{2-z}u_0\\
					= & R(z)u_0 \text{,}
				\end{align*}
				where,
				$$
				R(z) = \frac{2+z}{2-z}
				$$
				And the polynomial expansion of $R(z)$ is:
				\begin{align*}
					\frac{2+z}{2-z} = & (1+\frac{z}{2})(1+\frac{z}{2}+\frac{z^2}{4}+\frac{z^3}{8}+\cdots)\\
					= & 1 + z + \frac{1}{2}z^2 + \frac{1}{4} z^3 \cdots\\
					= & (1+\frac{z}{2})\sum_{n=0}^\infty (\frac{z}{2})^n\\
					= & 1+2\sum_{n=1}^\infty (\frac{z}{2})^n
				\end{align*}
				Compare $R(z)$ with the polynomial series, we could easily find that the step error is $O(k^3)$
				
				The A-stability region is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale = 0.5]{./figures/pb1d.eps}
					\caption{The A-stability region of trapezoidal scheme}
				\end{figure}
				The absolute stability is always guaranteed for trapezoidal scheme as long as $\lambda$ is real and negative.
				
      \item For Runge-Kutta 4 scheme, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c c c c}
						0 & 0\\
						1/2 & 1/2 & 0\\ 
						1/2 & 0 & 1/2 & 0\\
						1 & 0 & 0 & 1 & 0\\\hline
						$b_j$ & 1/6 & 1/3 & 1/3 & 1/6
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				\begin{align*}
					u_1 = & u_0 + \frac{1}{6} k\lambda (u_0 + 2(u_0+\frac{k}{2}\lambda u_0) + 2(u_0 +\frac{k}{2}\lambda(u_0 + \frac{k}{2}\lambda u_0))\\
					& + u_0 + k(u_0+\frac{k}{2}\lambda(u_0+\frac{k}{2}\lambda u_0))) \\
					\therefore u_1 = & (1+z+\frac{z^2}{2}+\frac{z^3}{6}+\frac{z^4}{24}) u_0\\
					= & R(z)u_0 \text{,}
				\end{align*}
				where,
				$$
				R(z) = 1 + z + \frac{z^2}{2} + \frac{z^3}{6} + \frac{z^4}{24}
				$$
				Compare $R(z)$ with the polynomial series, we could easily find that the step error is $O(k^5)$
				
				The A-stability region is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale = 0.5]{./figures/pb1e.eps}
					\caption{The A-stability region of RK4 scheme}
				\end{figure}
				If absolute stability is guaranteed, the value of $z$ should be no smaller than $-2.7853$ as shown in the stability plot. Thus the minimum time step $k$ should satisfy
				\begin{align*}
					k \leqslant & -\frac{2.7853}{\lambda}
				\end{align*}
				
      \item For TR-BDF2 scheme, the butcher-tableau is:
				\begin{center}
					\begin{tabular}{ c | c c c}
						0 & 0\\
						1/2 & 1/4 & 1/4\\
						1 & 1/3 & 1/3 & 1/3\\ \hline
						$b_j$ & 1/3 & 1/3 & 1/3 
					\end{tabular}
				\end{center}
				The explicit stepping schemes for one step of size $k$ is as follows:
				\begin{align*}
					u_{1/2} = & u_0 + \frac{k}{4}(\lambda u_0+\lambda u_{1/2})\\
					\therefore u_{1/2} = & \frac{4+z}{4-z} u_0\\
					u_1 = & \frac{1}{3}(4u_{1/2} - u_0 +k\lambda u_1) \\
					\therefore u_1 = & \frac{12 + 5z}{(3-z)(4-z)} u_0\\
					= & R(z)u_0 \text{,}
				\end{align*}
				where,
				\begin{align*}
					R(z) = & \frac{12 + 5z}{(3-z)(4-z)}\\
					= & 1 + z + \frac{z^2}{2} + \frac{5z^3}{24} + \cdots
				\end{align*}
				Compare $R(z)$ with the polynomial series, we could easily find that the step error is $O(k^3)$
				
				The A-stability region is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale = 0.5]{./figures/pb1f.eps}
					\caption{The A-stability region of TR-BDF2 scheme}
				\end{figure}
				As shown in the graph, all $k$'s will guarantee the absolute stability as long as $\lambda$ is negative.

    \end{enumerate}

	\item Basic ODE Fun
    \begin{enumerate}[\hspace{-0.5in} \bf {Part} (a)]
			\item {\bf Solution}: First, we need to calculate the expression of $\mathbf V$:
				\begin{align*}
					{\mathbf V} = & \nabla\times\psi{\mathbf k} \\
					= & (\frac{\partial}{\partial x},\frac{\partial}{\partial y},\frac{\partial}{\partial z})^T \times (0,0,\sin(\pi x/2)\sin(\pi y))\\
					= & (\frac{\partial \sin(\pi x/2)\sin(\pi y)}{\partial y},-\frac{\partial \sin(\pi x/2)\sin(\pi y)}{\partial x},0)\\
					= & (\pi\sin(\pi x/2)\cos(\pi y),-\frac{\pi}{2}\cos(\pi x/2)\sin(\pi y),0)
				\end{align*}
				Using the expression of ${\mathbf V}$, a simple program is written as follows to solve the vector ode using forward Euler scheme.
				\lstinputlisting[style=customPython]{./pythonFiles/forwardEuler.py}
				Forward Euler's scheme is not expected to be stable, as we could linearize the right hand side locally using Jacobi matrix:
				\begin{align*}
					{\mathbf V} = J {\mathbf x}\text{,}
				\end{align*}
				where,
				\begin{align*}
					J = & 
					\begin{bmatrix}
						\displaystyle \frac{\partial V_x}{\partial x} & \displaystyle \frac{\partial V_x}{\partial y} \\[5pt]
						\displaystyle \frac{\partial V_y}{\partial x} & \displaystyle \frac{\partial V_y}{\partial y}
					\end{bmatrix}\\
					= &
					\begin{bmatrix}
						\displaystyle \frac{\pi^2}{2}\cos(\pi x/2)\cos(\pi y) & -\pi^2\sin(\pi x/2)\sin(\pi y)\\[5pt]
						\displaystyle \frac{\pi^2}{4}\sin(\pi x/2)\sin(\pi y) & \displaystyle -\frac{\pi^2}{2}\cos(\pi x/2)\cos(\pi y)
					\end{bmatrix}
				\end{align*}
				Solving for the eigenvalues of this Jacobi matrix, we could find that:
				\begin{align*}
					\lambda^2 - \frac{\pi^4}{4}(\sin^2(\frac{\pi}{2}x)\sin^2(\pi y) - \cos^2(\frac{\pi}{2} x)\cos^2(\pi y))
				\end{align*}
				It is easy to see that two solutions of $\lambda$ is always symmetric according to the original point. Thus the two eigenvalues can not be in the stable area (as shown in Problem 1 of forward Euler scheme) simultaneously. Thus the scheme is not stable.
			\item {\bf Solution}: Modify the python program a little bit to draw a convergence plot $r(t)|_{t=15}$ vs the number of time steps. The code and the plot is shown respectively in the following part:
				\lstinputlisting[style=customPython]{./pythonFiles/forwardEulerConverge.py}
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.5]{./figures/pb2b.png}
					\caption{The convergence plot of relative error $r(t)$ vs number of steps}
				\end{figure}
				From the figure, we could easily see that in order to make $|r|<10^{-3}$, we approximately need an $Nstep \approx 1.45\times10^6$

			\item Modify the python file ``stupidode.py'' to add the RK2 scheme into the solver. At this time, according to the argument made in Part (a), and the A-stability region shown in Problem 1 for RK2 scheme, this scheme is not guaranteed absolute stability as well. However, the relative error is much better than that of forward Euler.
				The convergence plot for $t=15$ with different number of steps is shown as follows.
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.5]{./figures/pb2c.png}
					\caption{The convergence plot of relative error $r(t)$ vs number of steps}
				\end{figure}
				From the plot, we could find that it takes only about $Nstep\approx 10^4$ to make the error $|r|<10^{-3}$.
			
			\item {\bf Extra Credit}: Further modify the python file ``stupidode.py'' to add BDF-1 scheme into the solver. Using BDF-1 to solve this problem, we could find that the relative error remain constant as we take different number of steps.
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.5]{./figures/pb2d.png}
					\caption{The convergence plot of relative error $r(t)$ vs number of steps}
				\end{figure}
				If we look into what happened during solving this problem, we could find easily find that the solution goes to (1,0.5) in a few steps. At the point, the analytical value of right hand side $\mathbf V = \mathbf 0$. Thus it is the fix point of this problem (in fact, it is the center of the stream). From this, we could see that although backward Euler is always absolute stable, but it could possibly give a strange answer that far from the true solution.

				The final modified file ``stupidode.py'' is shown as follows:
				\lstinputlisting[style=customPython]{./pythonFiles/stupidode.py}

			\item Modify the python program into ``streamFunction.py'' (which will be shown in the following of the solution), we could solve the same problem using built in ODE integrator. At $t=15$, the relative error generated by Dormand Prince 56 BDF scheme is $3.875\times 10^{-6}$, and that of Vode BDF scheme is $4.500\times 10^{-4}$. The trajectory the particle moves along is shown by the following two graphs: 
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.5]{./figures/pb2e1.png}
					\caption{The trajectory of the particle in Dormand Prince 56 BDF scheme}
				\end{figure}
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.5]{./figures/pb2e2.png}
					\caption{The trajectory of the particle in Vode BDF scheme}
				\end{figure}
				From the figures, we could see that the particle follows the contour relatively well in these two schemes. 
		\end{enumerate}

\end{enumerate}

\end{document}
