# -*- coding: utf-8 -*-
"""
Created on Sat Nov  2 22:06:33 2013

@author: mspieg
"""
import numpy as np

class stupidode:
    """ simple wrapper class for hand rolled single stepper schemes that mimic
    the scipy ode class:
    
    Usage:  ode = stupidode(f)  # initialize an ode solver with rhs f(t,y)
            ode.set_integrator(method,nsteps)
            ode.set_initial_value(y0,t0)
            ode.integrate(t_max)
    """
    def __init__(self,f):
        """Initialize the ode object and set the rhs f
        """
        self.f=f
        self.y0 = np.array([0])      
    def set_integrator(self,method,nsteps=1):
        """ set the integration scheme:
        method is the stepper (currently only euler)
        nsteps is the number of fixed sized steps taken over an integration
        """
        self.method = method
        self.nsteps = nsteps
    def set_initial_value(self,y0,t0):
        """ set the initial condition y0,t0
        """
        self.y0 = y0.copy()
        self.t0 = t0
        self.y = y0
        self.t = t0
    def integrate(self,t):
        """take nstep steps from t0 to t (ugly!) 
        """
        if self.method == 'euler':
            nsteps = self.nsteps
            tn = np.linspace(self.t0,t,nsteps+1)
            k  = np.diff(tn)
            f = self.f
            for i in xrange(nsteps):
                self.y += k[i]*f(self.t,self.y)
                self.t = tn[i+1] 
        else:
            return "Error:  method ",self.method," is not supported"
            
# now test the class

def func(t,y):
    return -y
    
ode = stupidode(func)
ode.set_integrator('euler',nsteps=10)
ode.set_initial_value(np.array([1.]),0.)
ode.integrate(1.)
print ode.t, ode.y

    
    