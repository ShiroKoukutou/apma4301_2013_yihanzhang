% English Version of fe-i-tex

\documentclass{article}

% Set margins:
\hoffset = -0.9in
\voffset = -0.75in
\textheight = 680pt
\textwidth = 470pt

% Mathematic libraries
\usepackage{amsmath}
\usepackage{amssymb}
\newcommand{\diff}{\mathrm{d}}
\newcommand{\me}{\mathrm{e}}
\newcommand{\mi}{\mathrm{i}}
\newcommand{\erf}{\mathrm{erf}}

% Symbol libraries
\usepackage{textcomp}

% Code display library
\usepackage{mylistings}

% Graphic libraries
\usepackage{float}
\usepackage{graphicx}

% Enumerate label libraries
\usepackage{enumerate}

% Font package
\usepackage{times}

\usepackage{setspace}

% TODO: Add your article here.

\begin{document}

\title{Numerical Method for Partial Differential Equations:\\ Solution to Homework \#6}
\author{Yihan Zhang (Uni: yz2567)}
\maketitle

\begin{enumerate}[\bfseries \large 1.]
  \item {\bf Solution}: Plug the function
		$$
		T(\mathbf x, t) = \frac{A}{1+4t/\sigma^2}exp[\frac{-\mathbf x^T \mathbf x}{\sigma^2+4t}]
		$$
		back to the equation, we can get:
		\begin{align*}
			\text{Left hand side} = & \frac{\partial T}{\partial t}\\[5pt]
			= & - \frac{4}{\sigma^2} \frac{A}{(1+4t/\sigma^2)^2}exp[\frac{-\mathbf x^T \mathbf x}{\sigma^2+4t}] - \frac{A}{1+4t/\sigma^2}4\frac{-\mathbf x^T \mathbf x}{(\sigma^2+4t)^2}-exp[\frac{-\mathbf x^T \mathbf x}{\sigma^2+4t}]\\[5pt]
			= & (\mathbf x^T \mathbf x - \sigma^2 - 4t)\frac{4 A \sigma^2}{(\sigma^2+4t)^3}exp[\frac{-\mathbf x^T \mathbf x}{\sigma^2+4t}]\\[5pt]
			\text{Right hand side} = & \nabla^2 T = \frac{\partial^2 T}{\partial x_1^2} + \frac{\partial^2 T}{\partial x_2^2}\\[5pt]
			= & \frac{A}{1+4t/\sigma^2}exp[\frac{-\mathbf x^T \mathbf x}{\sigma^2+4t}]( \frac{4x_1^2}{(\sigma^2+4t)^2} - \frac{2}{\sigma^2+4t} + \frac{4x_2^2}{(\sigma^2+4t)^2} - \frac{2}{\sigma^2+4t} )\\[5pt]
			= & (\mathbf x^T \mathbf x - \sigma^2 - 4t)\frac{4 A \sigma^2}{(\sigma^2+4t)^3}exp[\frac{-\mathbf x^T \mathbf x}{\sigma^2+4t}]\\
		\end{align*}
		Therefore, left hand side equals to right hand side. The assumed function is the solution to diffusion problem.

	\item Numerical Method
    \begin{enumerate}[\bf {Part} (a)]
			\item The weak form for the equation given a test function $T_t$ and an estimation for the new time step $T_{n+1}$ is:
				\begin{align*}
					F[T_n] = & \int_\Omega T_t(T_{n+1}-T_n - k \nabla^2 T_\theta) \diff\mathbf x\\
					= & \int_\Omega T_t(T_{n+1}-T_n) + k\nabla T_t \cdot \nabla T_\theta \diff \mathbf x + k\int_{\partial \Omega} T_t \nabla T_\theta \cdot \hat{\mathbf n} \diff S\\
					= & \int_\Omega T_t(T_{n+1}-T_n) + k\nabla T_t \cdot \nabla \big( (1-\theta)T_n +\theta T_{n+1} \big) \diff \mathbf x \quad \text{as} \ \nabla T \cdot \hat{\mathbf n} = 0 \ \text{on the boundaries}
				\end{align*}
			\item The Jacobian equals to
				\begin{align*}
					J[T_n] = & \delta F[T_n]\\
					= & \lim_{\epsilon\rightarrow 0}\frac{F[T_n+\epsilon\delta T_n]-F[T_n]}{\epsilon}\\
					= & \int_\Omega \big( -T_t\delta T_n + k(1-\theta) \nabla T_t \cdot \nabla\delta T_n \big) \diff \mathbf x
				\end{align*}
			\item In order to modify the program to solve for $\theta$ scheme, we need to add a coefficient called ``Theta'', which has a constant value, as well as modify the definition of ``Ttheta'' in ``Residual'' to
				$$
				T_\theta = (1-\theta)T_n+\theta T_{half_i}
				$$
			\item For forward Euler scheme, using A-stability analysis, we could find that if the scheme is absolutely stable, thus it should satisfy
				$$
				1+k\lambda\leq 1.
				$$
				$\lambda$ above stands for the eigenvalues of the mass matrix, which is assembled using $P1$ elements. Thus as all eigenvalues are negative, the time step should satisfy
				$$
				k\leq \frac{h^2}{4}.
				$$
				Therefore, the maximum time step that ensures the problem to be stable is $k=2.5\times 10^{-5}$. For that $k$, the $L1$ norm error is shown as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/FE_000025.png}
					\caption{L1-norm error against number of iterations, $dt=2.5\times 10^{-5}$}
				\end{figure}
				We could see that the L1 error does not converge. Using bisection, we could approximately find an time step $dt=1\times 10^{-6}$ where the L1-norm error converges slowly as shown in the following figure.
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/FE_000001.png}
					\caption{L1-norm error against number of iterations, $dt=1\times 10^{-6}$}
				\end{figure}
				Compare that of TRBDF-2 (which is shown in Fig.3), we could find that TRBDF-2 has a much smaller L1 error. That is because forward Euler scheme has a global error of $O(k)$, and TRBDF-2 has a global error of $O(k^2)$.
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/TRBDF2.png}
					\caption{L1-norm error against number of iterations, TRBDF-2 scheme, $dt=2\times 10^{-4}$}
				\end{figure}

			\item From the figure, we could find that the $L1$ norm error for backward Euler (BDF1) is $1.1538\times 10^{-10}$ at time $t=0.012$ with time step $dt=0.0002$. And $L1$ norm error for BDF2 scheme with the same parameter is $0.833\times 10^{-10}$. This might because this scheme is unconditionally stable, thus the amplitude all frequency decays fast as the exact solution to the problem is. As the global error of backward Euler is $O(k)$, thus if we make the time step $dt=0.0002/1.1538*0.833\approx 0.00014$, the solution is expected to have an $L1$ norm error approximately equal to $0.833\times 10^{-10}$. However, the result is not the same as predicted. The error is $1.1426\times 10^{-10}$ using $dt=0.00014$.
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/BE_0002.png}
					\caption{L1-norm error against number of iterations, BDF-1 scheme, $dt=2\times 10^{-4}$}
				\end{figure}
				However, after several times of trial, we could approximately find a time step that generates a error approximately equal to that given by TRBDF-2 scheme. The time step is about $dt=1\times 10^{-5}$, and the error is about $0.9\times 10^{-10}$, as shown in the following figure.
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/BE_000001.png}
					\caption{L1-norm error against number of iterations, BDF-1 scheme, $dt=1\times 10^{-6}$}
				\end{figure}

			\item For Crank-Nicolson scheme, the $L1$ norm error plot is shown as follows when $dt=2\times 10^{-4}$:
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/CN_0002.png}
					\caption{L1-norm error against number of iterations, Crank-Nicolson scheme, $dt=2\times 10^{-4}$}
				\end{figure}
				We can see that the $L1$ norm error is better than that of BDF-1, and a little worse than TRBDF-2, which is accord to their desired behavior. If we use smaller time step, we could get a smaller error that may approximately equal to that of TRBDF-2. Using the same trick in Part (e), we could guess a new time step $dt=0.0002/\sqrt{1.19/0.83}\approx 0.000167$. A square root is used because both Crank-Nicolson and TRBDF-2 are second order schemes. That will give an $L1$ norm error as follows:
				\begin{figure}[H]
					\centering
					\includegraphics[scale=0.35]{./figures/CN_00016.png}
					\caption{L1-norm error against number of iterations, Crank-Nicolson scheme, $dt=8\times 10^{-5}$}
				\end{figure}
				This time, the error is comparable to that of TRBDF-2.

		\end{enumerate}

\end{enumerate}

\end{document}
