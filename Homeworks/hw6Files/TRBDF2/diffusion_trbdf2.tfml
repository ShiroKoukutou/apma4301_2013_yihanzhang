<?xml version='1.0' encoding='utf-8'?>
<terraferma_options>
  <geometry>
    <dimension>
      <integer_value rank="0">2</integer_value>
    </dimension>
    <mesh name="Mesh">
      <source name="Rectangle">
        <lower_left>
          <real_value shape="2" dim1="2" rank="1">0. 0.</real_value>
        </lower_left>
        <upper_right>
          <real_value shape="2" dim1="2" rank="1">1. 1.</real_value>
        </upper_right>
        <number_cells>
          <integer_value shape="2" dim1="2" rank="1">100 100</integer_value>
        </number_cells>
        <diagonal>
          <string_value>right/left</string_value>
        </diagonal>
        <cell>
          <string_value>triangle</string_value>
        </cell>
      </source>
    </mesh>
  </geometry>
  <io>
    <output_base_name>
      <string_value lines="1">diffusion</string_value>
    </output_base_name>
    <visualization>
      <element name="P1">
        <family>
          <string_value>CG</string_value>
        </family>
        <degree>
          <integer_value rank="0">1</integer_value>
        </degree>
      </element>
    </visualization>
    <dump_periods>
      <visualization_period>
        <real_value rank="0">.0002</real_value>
      </visualization_period>
    </dump_periods>
    <detectors/>
  </io>
  <timestepping>
    <current_time>
      <real_value rank="0">0.</real_value>
    </current_time>
    <finish_time>
      <real_value rank="0">.012</real_value>
    </finish_time>
    <timestep>
      <coefficient name="Timestep">
        <ufl_symbol>
          <string_value lines="1">dt</string_value>
        </ufl_symbol>
        <type name="Constant">
          <rank name="Scalar" rank="0">
            <value name="WholeMesh">
              <constant>
                <real_value rank="0">.0002</real_value>
                <comment>h ~ 0.01 

dt &lt;= beta*h^2</comment>
              </constant>
            </value>
          </rank>
        </type>
      </coefficient>
    </timestep>
  </timestepping>
  <global_parameters/>
  <system name="Diffusion">
    <mesh name="Mesh"/>
    <ufl_symbol>
      <string_value lines="1">us</string_value>
    </ufl_symbol>
    <field name="T">
      <ufl_symbol>
        <string_value lines="1">T</string_value>
      </ufl_symbol>
      <type name="Function">
        <rank name="Scalar" rank="0">
          <element name="P1">
            <family>
              <string_value>CG</string_value>
            </family>
            <degree>
              <integer_value rank="0">1</integer_value>
            </degree>
          </element>
          <initial_condition type="initial_condition" name="WholeMesh">
            <python rank="0">
              <string_value lines="20" type="code" language="python">from math import exp

def gaussian(x,t,amp,sigma,x0,y0):
  ''' general routine for calculating a 2-D Gaussian
      amp: amplitude
      sigma: half-width
      x0,y0: coordinate of peak
  ''' 
  global exp
  sigma_0_squared = sigma*sigma
  sigma_squared = sigma_0_squared +4.*t
  ampt = amp*sigma_0_squared/sigma_squared

  r_squared = (x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0)
  return ampt*exp(-r_squared/sigma_squared)
  
def val(x):
  global gaussian
  return gaussian(x,0,1.,0.05,0.5,0.5)
</string_value>
            </python>
          </initial_condition>
        </rank>
      </type>
      <diagnostics>
        <include_in_visualization/>
        <include_in_statistics>
          <functional name="MeanT">
            <string_value lines="20" type="code" language="python">meanT = T_i*dx</string_value>
            <ufl_symbol>
              <string_value lines="1">meanT</string_value>
            </ufl_symbol>
            <quadrature_rule name="default"/>
          </functional>
          <functional name="L1Err">
            <string_value type="code" lines="20" language="python">L1err=abs(T_i - Ta_i)*dx</string_value>
            <ufl_symbol>
              <string_value lines="1">L1err</string_value>
            </ufl_symbol>
            <quadrature_rule name="default"/>
          </functional>
          <functional name="L1norm">
            <string_value lines="20" type="code" language="python">L1norm=abs(T_i)*dx</string_value>
            <ufl_symbol>
              <string_value lines="1">L1norm</string_value>
            </ufl_symbol>
            <quadrature_rule name="default"/>
          </functional>
          <functional name="L2ErrSquared">
            <string_value lines="20" type="code" language="python">L2errSquared=(T_i - Ta_i)*(T_i - Ta_i)*dx</string_value>
            <ufl_symbol>
              <string_value lines="1">L2errSquared</string_value>
            </ufl_symbol>
            <quadrature_rule name="default"/>
          </functional>
        </include_in_statistics>
      </diagnostics>
    </field>
    <field name="Thalf">
      <ufl_symbol>
        <string_value lines="1">Thalf</string_value>
      </ufl_symbol>
      <type name="Function">
        <rank name="Scalar" rank="0">
          <element name="P1">
            <family>
              <string_value>CG</string_value>
            </family>
            <degree>
              <integer_value rank="0">1</integer_value>
            </degree>
          </element>
          <initial_condition type="initial_condition" name="WholeMesh">
            <constant>
              <real_value rank="0">0.</real_value>
            </constant>
          </initial_condition>
        </rank>
      </type>
      <diagnostics>
        <include_in_visualization/>
        <include_in_statistics>
          <functional name="MeanT">
            <string_value lines="20" type="code" language="python">meanT = T_i*dx</string_value>
            <ufl_symbol>
              <string_value lines="1">meanT</string_value>
            </ufl_symbol>
            <quadrature_rule name="default"/>
          </functional>
        </include_in_statistics>
      </diagnostics>
    </field>
    <coefficient name="AnalyticSolution">
      <ufl_symbol>
        <string_value lines="1">Ta</string_value>
      </ufl_symbol>
      <type name="Expression">
        <rank name="Scalar" rank="0">
          <element name="P1">
            <family>
              <string_value>CG</string_value>
            </family>
            <degree>
              <integer_value rank="0">1</integer_value>
            </degree>
          </element>
          <value type="value" name="WholeMesh">
            <python rank="0">
              <string_value lines="20" type="code" language="python">def val(x,t):
  global gaussian
  return gaussian(x,t,1.,0.05,0.5,0.5)
</string_value>
            </python>
          </value>
        </rank>
      </type>
      <diagnostics>
        <include_in_visualization/>
      </diagnostics>
    </coefficient>
    <nonlinear_solver name="TRBDF2">
      <type name="SNES">
        <form name="Residual" rank="0">
          <string_value lines="20" type="code" language="python">Ttheta = 0.5*(T_n + Thalf_i)
F = (Thalf_t*(Thalf_i - T_n) + 0.5*dt*inner(grad(Thalf_t),grad(Ttheta)))*dx
F += (T_t*(3.*T_i -4.*Thalf_i + T_n) + dt*inner(grad(T_t),grad(T_i)))*dx</string_value>
          <comment>Residual for the TR step for dt/2</comment>
          <ufl_symbol>
            <string_value lines="1">F</string_value>
          </ufl_symbol>
        </form>
        <form name="Jacobian" rank="1">
          <string_value lines="20" type="code" language="python">J = derivative(F,us_i,us_a)</string_value>
          <ufl_symbol>
            <string_value lines="1">J</string_value>
          </ufl_symbol>
        </form>
        <quadrature_rule name="default"/>
        <snes_type name="ksponly"/>
        <relative_error>
          <real_value rank="0">1.e-7</real_value>
        </relative_error>
        <absolute_error>
          <real_value rank="0">1.e-10</real_value>
        </absolute_error>
        <max_iterations>
          <integer_value rank="0">2</integer_value>
        </max_iterations>
        <monitors>
          <view_snes/>
          <residual/>
        </monitors>
        <linear_solver>
          <iterative_method name="richardson">
            <relative_error>
              <real_value rank="0">1.e-7</real_value>
            </relative_error>
            <max_iterations>
              <integer_value rank="0">10</integer_value>
            </max_iterations>
            <zero_initial_guess/>
            <monitors>
              <preconditioned_residual/>
            </monitors>
          </iterative_method>
          <preconditioner name="fieldsplit">
            <composite_type name="multiplicative"/>
            <fieldsplit name="Thalf">
              <field name="Thalf"/>
              <monitors/>
              <linear_solver>
                <iterative_method name="cg">
                  <relative_error>
                    <real_value rank="0">1.e-7</real_value>
                  </relative_error>
                  <absolute_error>
                    <real_value rank="0">1.e-9</real_value>
                  </absolute_error>
                  <max_iterations>
                    <integer_value rank="0">20</integer_value>
                  </max_iterations>
                  <zero_initial_guess/>
                  <monitors>
                    <preconditioned_residual/>
                  </monitors>
                </iterative_method>
                <preconditioner name="hypre">
                  <hypre_type name="boomeramg"/>
                </preconditioner>
              </linear_solver>
            </fieldsplit>
            <fieldsplit name="T">
              <field name="T"/>
              <monitors/>
              <linear_solver>
                <iterative_method name="cg">
                  <relative_error>
                    <real_value rank="0">1.e-7</real_value>
                  </relative_error>
                  <absolute_error>
                    <real_value rank="0">1.e-10</real_value>
                  </absolute_error>
                  <max_iterations>
                    <integer_value rank="0">20</integer_value>
                  </max_iterations>
                  <zero_initial_guess/>
                  <monitors>
                    <preconditioned_residual/>
                  </monitors>
                </iterative_method>
                <preconditioner name="hypre">
                  <hypre_type name="boomeramg"/>
                </preconditioner>
              </linear_solver>
            </fieldsplit>
          </preconditioner>
        </linear_solver>
        <never_ignore_solver_failures/>
      </type>
    </nonlinear_solver>
    <solve name="in_timeloop"/>
  </system>
</terraferma_options>
