clc;
clear all;
close all;

% Set the scale this demostration will cover:
Scale = 10;
PeArray = 10.^(-Scale/2:Scale/2-1);

% Set the number of sample:
SampleNumber = 1e4;
x = linspace(0,1,SampleNumber);
i = ones(SampleNumber,1);

% Generate the resulting function in tensor space:
PeTensor = PeArray'*i';
T = 1./(1-exp(PeTensor)).*(exp(PeArray'*x)-exp(PeTensor));

figure;
hold on;
for k=1:Scale
    plot(x,T(k,:),'Color',[0.8 - 0.8/Scale*k,0.8 - 0.8/Scale*k,0.8 - 0.8/Scale*k]);
end
xlabel('x''');
ylabel('T''_s(x'')');
title('T''_s(x'') against x'' under different Pe''s');
hold off;