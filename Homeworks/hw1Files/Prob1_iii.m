clc;
clear all;
close all;

% Assume m=2p-1, n=2q-1 and N=2M-1
% This program is used to plot the figure of truncation error and Norm-2
% error against M.

% Plot from M=1 to M=100
M = 300;

% m=2p-1 and n=2q-1 sequance:
sequenceP = 1:2:2*M-1;
sequenceQ = 1:2:2*M-1;

% Let p increases vertically and q horizontally:
pField = sequenceP'*ones(1,M);
qField = ones(1,M)'*sequenceQ;

% Generate the square field of m^2+n^2
squareField = (pField).^2 + (qField).^2;

% Generate the a_pq matrix, with constants removed
Apq = 8./(pField.*qField.*squareField.*pi^4);

% The square of Apq
ApqSquare = Apq.^2;

% The sum array should be the sum of all numbers in the nxn square of Apq
sumArray = zeros(1,100);
for k = 1:M
    sumArray(k)=sum(sum(ApqSquare(1:k,1:k)));
end 

% First, use the Apq^2 array to get r_N
rN = (diff(sumArray)./sumArray(2:end)).^0.5;

rNL2 = diff(sumArray.^0.5)./(sumArray(2:end).^0.5);

% Plot the two errors agains N:
figure;
semilogx(sequenceP(2:end),log10(rN));
title('Relative truncation error r_N as a function of N')
xlabel('N=2M-1');
ylabel('log(r_N)');
axis([1 1000000 -16 0])

figure;
semilogx(sequenceP(2:end),log10(rNL2));
title('Relative error in the 2-norm r_{N(L2)} as a function of N')
xlabel('N=2M-1');
ylabel('log(r_{N(L2)})');
